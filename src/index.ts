/* eslint-disable @typescript-eslint/no-var-requires */
import express = require("express");
import { createConnection } from "typeorm";
import routes from "./api/routes";
import bodyParser = require("body-parser");
import { getConfig } from "./config/config";
import * as cors from "cors";
import * as helmet from "helmet";
import { setup, serve } from "swagger-ui-express";
import * as swaggerDoc from "swagger-jsdoc";
import { swaggerOptions } from "./config/config.swagger";

createConnection()
  .then(async () => {
    // Create a new express application instance
    const app: express.Application = express();
    const whitelist = getConfig("ALLOWED_ORIGIN");
    const corsOptions = {
      origin: function(origin: string, callback) {
        if (
          whitelist.indexOf(origin) !== -1 ||
          !origin ||
          origin.startsWith("http://localhost")
        ) {
          callback(null, true);
        } else {
          callback(new Error("Not allowed by CORS"));
        }
      },
      optionsSuccessStatus: 200
    };

    // Call midlewares
    app.use(cors(corsOptions));
    app.use(helmet());
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));

    //Set all routes from routes folder
    app.use("/", routes);

    // Set documentation route
    const swaggerDocs = swaggerDoc(swaggerOptions);
    app.use("/api-docs", serve, setup(swaggerDocs));

    app.listen(getConfig("PORT"), () => {
      console.log(`Server started on port ${getConfig("PORT")}!`);
    });
  })
  .catch(err => console.log(err));
