import { UserModel } from "../docs/models/user.model.doc";
import { UserRoleModel } from "../docs/models/userRole.model.doc";
import { ComponentModel } from "../docs/models/component.model.doc";
import { ComponentGroupModel } from "../docs/models/componentGroup.model.doc";
import { ElementCharacteristicModel } from "../docs/models/elementCharacteristic.model.doc";
import { ElementNatureModel } from "../docs/models/elementNature.model.doc";
import { ElementUsageUnitModel } from "../docs/models/elementUsageUnit.model.doc";
import { ProviderModel } from "../docs/models/provider.model.doc";
import { CustomerModel } from "../docs/models/customer.model.doc";
import { CrossSectionModel } from "../docs/models/crossSection.model.doc";
import { PaymentMethodModel } from "../docs/models/paymentMethod.model.doc";
import { QuoteModel } from "../docs/models/quote.model.doc";
import { ProjectModel } from "../docs/models/project.model.doc";
import { QuoteStateModel } from "../docs/models/quoteState.model.doc";
import { UserGlobal, UserCurrent } from "../docs/routes/user/user.global.doc";
import { AuthGlobal } from "../docs/routes/authentication/auth.global.doc";
import { ProviderGlobal } from "../docs/routes/provider/provider.global.doc";
import { ProviderById } from "../docs/routes/provider/provider.byId.doc";
import { CustomerById } from "../docs/routes/customer/customer.byId.doc";
import { ElementUsageUnitById } from "../docs/routes/element/elementUsageUnit.byId.doc";
import { ElementUsageUnitGlobal } from "../docs/routes/element/elementUsageUnit.global.doc";
import { ElementCharacteristicById } from "../docs/routes/element/elementCharacteristic.byId.doc";
import { ElementCharacteristicGlobal } from "../docs/routes/element/elementCharacteristic.global.doc";
import { ElementNatureById } from "../docs/routes/element/elementNature.byId.doc";
import { ElementNatureGlobal } from "../docs/routes/element/elementNature.global.doc";
import { ComponentGlobal } from "../docs/routes/component/component.global.doc";
import { ComponentById } from "../docs/routes/component/component.byId.doc";
import { CrossSectionGlobal } from "../docs/routes/quote/crossSection.global.doc";
import { CrossSectionById } from "../docs/routes/quote/crossSection.byId.doc";
import { PaymentMethodGlobal } from "../docs/routes/quote/paymentMethod.global.doc";
import { PaymentMethodById } from "../docs/routes/quote/paymentMethod.byId.doc";
import { ComponentGroupById } from "../docs/routes/component/componentGroup.byId.doc";
import { ComponentGroupGlobal } from "../docs/routes/component/componentGroup.global.doc";
import { CustomerGlobal } from "../docs/routes/customer/customer.global.doc";
import { ProjectGlobal } from "../docs/routes/project/project.global.doc";
import { ProjectById } from "../docs/routes/project/project.byId.doc";
import { QuoteStateById } from "../docs/routes/quote/quoteState.byId.doc";
import { QuoteStateGlobal } from "../docs/routes/quote/quoteState.global.doc";
import { QuoteById } from "../docs/routes/quote/quote.byId.doc";
import { QuoteGlobal } from "../docs/routes/quote/quote.global.doc";
import { UserById } from "../docs/routes/user/user.byId.doc";
import { UserRoleById } from "../docs/routes/user/userRole.byId.doc";
import { UserRoleGlobal } from "../docs/routes/user/userRole.global.doc";
import * as swaggerDoc from "swagger-jsdoc";
import { getConfig } from "./config";

// Swagger Config : https://swagger.io/specification/#infoObject
export const swaggerOptions: swaggerDoc.Options = {
  swaggerDefinition: {
    openapi: "3.0.0",
    basePath: "/",
    info: {
      title: "Madera API",
      description: "Customer API Information",
      version: "1.0.0"
    },
    components: {
      securitySchemes: {
        bearer: {
          type: "http",
          scheme: "bearer",
          bearerFormat: "JWT"
        }
      },
      schemas: {
        ElementCharacteristicModel,
        ElementUsageUnitModel,
        ElementNatureModel,
        UserModel,
        UserRoleModel,
        ProviderModel,
        CrossSectionModel,
        CustomerModel,
        PaymentMethodModel,
        ComponentModel,
        ComponentGroupModel,
        QuoteModel,
        QuoteStateModel,
        ProjectModel
      }
    },
    definitions: {
      UserGlobal,
      CustomerGlobal,
      UserCurrent,
      UserById,
      CustomerById,
      UserRoleById,
      UserRoleGlobal,
      CrossSectionGlobal,
      CrossSectionById,
      PaymentMethodGlobal,
      PaymentMethodById,
      ComponentGlobal,
      ComponentById,
      ElementCharacteristicGlobal,
      ElementCharacteristicById,
      ElementNatureGlobal,
      ElementNatureById,
      ElementUsageUnitGlobal,
      ElementUsageUnitById,
      ComponentGroupGlobal,
      ComponentGroupById,
      AuthGlobal,
      ProviderGlobal,
      ProviderById,
      QuoteGlobal,
      QuoteById,
      QuoteStateGlobal,
      QuoteStateById,
      ProjectGlobal,
      ProjectById
    },
    servers: [
      {
        url: `http://localhost:${getConfig("PORT")}`,
        description: "Lien pour le dev"
      },
      {
        url: `https://back-madera.herokuapp.com`,
        description: "Lien pour la prod"
      }
    ]
  },
  apis: ["**/*.ts"]
};
