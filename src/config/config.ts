const config = {
  PORT: process.env.PORT || 3000,
  ALLOWED_ORIGIN: [
    "https://front-madera.yteruel.fr",
    "https://back-madera.yteruel.fr"
  ]
};

type Key = keyof typeof config;

export const getConfig = <K extends Key>(key: K) => {
  return config[key];
};
