export const ElementUsageUnitGlobal = {
  get: {
    summary: "Récupère la liste complète des éléments d'unités d'usagess",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Composant"],
    responses: {
      200: {
        description: "Liste de toutes les éléments d'unités d'usagess",
        content: {
          "application/json": {
            schema: {
              type: "array",
              items: {
                $ref: "#/components/schemas/ElementUsageUnitModel"
              }
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  }
};
