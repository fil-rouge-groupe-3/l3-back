import { IdParams } from "../../utils.doc";

export const ElementUsageUnitById = {
  get: {
    summary:
      "Récupère une élément d'unité d'usage en fonction de l'id de celui-ci",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Composant"],
    parameters: [IdParams],
    responses: {
      200: {
        description: "Liste un élément d'unité d'usage",
        content: {
          "application/json": {
            schema: {
              $ref: "#/components/schemas/ElementUsageUnitModel"
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  },
  post: {
    summary: "Ajoute un nouvel élément d'unité d'usage",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Composant"],
    parameters: [IdParams],
    requestBody: {
      required: true,
      content: {
        "application/x-www-form-urlencoded": {
          schema: {
            $ref: "#/components/schemas/ElementUsageUnitModel"
          }
        }
      }
    },
    responses: {
      201: {
        description: "Element bien ajouté",
        content: {
          "application/json": {
            schema: {
              type: "object",
              properties: {
                data: {
                  $ref: "#/components/schemas/ElementUsageUnitModel"
                },
                message: {
                  type: "string",
                  example: "OK"
                }
              }
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  },
  put: {
    summary: "Modifie une élément d'unité d'usage",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Composant"],
    parameters: [IdParams],
    requestBody: {
      required: true,
      content: {
        "application/x-www-form-urlencoded": {
          schema: {
            $ref: "#/components/schemas/ElementUsageUnitModel"
          }
        }
      }
    },
    responses: {
      200: {
        description: "Element bien modifié",
        content: {
          "application/json": {
            schema: {
              type: "object",
              properties: {
                data: {
                  $ref: "#/components/schemas/ElementUsageUnitModel"
                },
                message: {
                  type: "string",
                  example: "1 modifié"
                }
              }
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  },
  delete: {
    summary: "Supprime une élément d'unité d'usage",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Composant"],
    parameters: [IdParams],
    responses: {
      200: {
        description: "La élément d'unité d'usage est bien supprimé",
        content: {
          "application/json": {
            schema: {
              type: "object",
              properties: {
                message: {
                  type: "string",
                  example: "1 supprimé"
                }
              }
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  }
};
