export const ElementNatureGlobal = {
  get: {
    summary: "Récupère la liste complète des éléments de natures",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Composant"],
    responses: {
      200: {
        description: "Liste de toutes les éléments de natures",
        content: {
          "application/json": {
            schema: {
              type: "array",
              items: {
                $ref: "#/components/schemas/ElementNatureModel"
              }
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  }
};
