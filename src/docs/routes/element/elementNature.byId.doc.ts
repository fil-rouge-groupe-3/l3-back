import { IdParams } from "../../utils.doc";

export const ElementNatureById = {
  get: {
    summary: "Récupère une élément de nature en fonction de l'id de celui-ci",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Composant"],
    parameters: [IdParams],
    responses: {
      200: {
        description: "Liste un élément de nature",
        content: {
          "application/json": {
            schema: {
              $ref: "#/components/schemas/ElementNatureModel"
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  },
  post: {
    summary: "Ajoute un nouvel élément de nature",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Composant"],
    parameters: [IdParams],
    requestBody: {
      required: true,
      content: {
        "application/x-www-form-urlencoded": {
          schema: {
            $ref: "#/components/schemas/ElementNatureModel"
          }
        }
      }
    },
    responses: {
      201: {
        description: "Element bien ajouté",
        content: {
          "application/json": {
            schema: {
              type: "object",
              properties: {
                data: {
                  $ref: "#/components/schemas/ElementNatureModel"
                },
                message: {
                  type: "string",
                  example: "OK"
                }
              }
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  },
  put: {
    summary: "Modifie une élément de nature",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Composant"],
    parameters: [IdParams],
    requestBody: {
      required: true,
      content: {
        "application/x-www-form-urlencoded": {
          schema: {
            $ref: "#/components/schemas/ElementNatureModel"
          }
        }
      }
    },
    responses: {
      200: {
        description: "Element bien modifié",
        content: {
          "application/json": {
            schema: {
              type: "object",
              properties: {
                data: {
                  $ref: "#/components/schemas/ElementNatureModel"
                },
                message: {
                  type: "string",
                  example: "1 modifié"
                }
              }
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  },
  delete: {
    summary: "Supprime une élément de nature",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Composant"],
    parameters: [IdParams],
    responses: {
      200: {
        description: "La élément de nature est bien supprimé",
        content: {
          "application/json": {
            schema: {
              type: "object",
              properties: {
                message: {
                  type: "string",
                  example: "1 supprimé"
                }
              }
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  }
};
