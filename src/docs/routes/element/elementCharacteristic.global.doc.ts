export const ElementCharacteristicGlobal = {
  get: {
    summary: "Récupère la liste complète des éléments de caractéristiques",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Composant"],
    responses: {
      200: {
        description: "Liste de toutes les éléments de caractéristiques",
        content: {
          "application/json": {
            schema: {
              type: "array",
              items: {
                $ref: "#/components/schemas/ElementCharacteristicModel"
              }
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  }
};
