import { IdParams } from "../../utils.doc";

export const ElementCharacteristicById = {
  get: {
    summary:
      "Récupère une élément de caractéristique en fonction de l'id de celui-ci",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Composant"],
    parameters: [IdParams],
    responses: {
      200: {
        description: "Liste un élément de caractéristique",
        content: {
          "application/json": {
            schema: {
              $ref: "#/components/schemas/ElementCharacteristicModel"
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  },
  post: {
    summary: "Ajoute un nouvel élément de caractéristique",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Composant"],
    parameters: [IdParams],
    requestBody: {
      required: true,
      content: {
        "application/x-www-form-urlencoded": {
          schema: {
            $ref: "#/components/schemas/ElementCharacteristicModel"
          }
        }
      }
    },
    responses: {
      201: {
        description: "Element bien ajouté",
        content: {
          "application/json": {
            schema: {
              type: "object",
              properties: {
                data: {
                  $ref: "#/components/schemas/ElementCharacteristicModel"
                },
                message: {
                  type: "string",
                  example: "OK"
                }
              }
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  },
  put: {
    summary: "Modifie une élément de caractéristique",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Composant"],
    parameters: [IdParams],
    requestBody: {
      required: true,
      content: {
        "application/x-www-form-urlencoded": {
          schema: {
            $ref: "#/components/schemas/ElementCharacteristicModel"
          }
        }
      }
    },
    responses: {
      200: {
        description: "Element bien modifié",
        content: {
          "application/json": {
            schema: {
              type: "object",
              properties: {
                data: {
                  $ref: "#/components/schemas/ElementCharacteristicModel"
                },
                message: {
                  type: "string",
                  example: "1 modifié"
                }
              }
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  },
  delete: {
    summary: "Supprime une élément de caractéristique",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Composant"],
    parameters: [IdParams],
    responses: {
      200: {
        description: "La élément de caractéristique est bien supprimé",
        content: {
          "application/json": {
            schema: {
              type: "object",
              properties: {
                message: {
                  type: "string",
                  example: "1 supprimé"
                }
              }
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  }
};
