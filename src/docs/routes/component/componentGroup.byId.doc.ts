import { IdParams } from "../../utils.doc";

export const ComponentGroupById = {
  get: {
    summary:
      "Récupère une famille de composant en fonction de l'id de celui-ci",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Famille de composant"],
    parameters: [IdParams],
    responses: {
      200: {
        description: "Liste une famille de composant",
        content: {
          "application/json": {
            schema: {
              $ref: "#/components/schemas/ComponentGroupModel"
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  },
  post: {
    summary: "Ajoute une nouvelle famille de composant",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Famille de composant"],
    parameters: [IdParams],
    requestBody: {
      required: true,
      content: {
        "application/x-www-form-urlencoded": {
          schema: {
            $ref: "#/components/schemas/ComponentGroupModel"
          }
        }
      }
    },
    responses: {
      201: {
        description: "Element bien ajouté",
        content: {
          "application/json": {
            schema: {
              type: "object",
              properties: {
                data: {
                  $ref: "#/components/schemas/ComponentGroupModel"
                },
                message: {
                  type: "string",
                  example: "OK"
                }
              }
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  },
  put: {
    summary: "Modifie une famille de composant",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Famille de composant"],
    parameters: [IdParams],
    requestBody: {
      required: true,
      content: {
        "application/x-www-form-urlencoded": {
          schema: {
            $ref: "#/components/schemas/ComponentGroupModel"
          }
        }
      }
    },
    responses: {
      200: {
        description: "Element bien modifié",
        content: {
          "application/json": {
            schema: {
              type: "object",
              properties: {
                data: {
                  $ref: "#/components/schemas/ComponentGroupModel"
                },
                message: {
                  type: "string",
                  example: "1 modifié"
                }
              }
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  },
  delete: {
    summary: "Supprime une famille de composant",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Famille de composant"],
    parameters: [IdParams],
    responses: {
      200: {
        description: "La famille de composant est bien supprimé",
        content: {
          "application/json": {
            schema: {
              type: "object",
              properties: {
                message: {
                  type: "string",
                  example: "1 supprimé"
                }
              }
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  }
};
