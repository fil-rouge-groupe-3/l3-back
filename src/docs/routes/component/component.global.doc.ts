export const ComponentGlobal = {
  get: {
    summary: "Récupère la liste complète des composants",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Composant"],
    responses: {
      200: {
        description: "Liste de toutes les composants",
        content: {
          "application/json": {
            schema: {
              type: "array",
              items: {
                $ref: "#/components/schemas/ComponentModel"
              }
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  }
};
