export const ComponentGroupGlobal = {
  get: {
    summary: "Récupère la liste complète des familles de composant",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Famille de composant"],
    responses: {
      200: {
        description: "Liste de toutes les familles de composant",
        content: {
          "application/json": {
            schema: {
              type: "array",
              items: {
                $ref: "#/components/schemas/ComponentGroupModel"
              }
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  }
};
