export const ProviderGlobal = {
  get: {
    summary: "Récupère la liste complète des fournisseurs",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Fournisseur"],
    responses: {
      200: {
        description: "Liste de tous les fournisseurs",
        content: {
          "application/json": {
            schema: {
              type: "array",
              items: {
                $ref: "#/components/schemas/ProviderModel"
              }
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  }
};
