import { IdParams } from "../../utils.doc";

export const ProviderById = {
  get: {
    summary: "Récupère un fournisseur en fonction de l'id de celui-ci",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Fournisseur"],
    parameters: [IdParams],
    responses: {
      200: {
        description: "Liste un fournisseur",
        content: {
          "application/json": {
            schema: {
              $ref: "#/components/schemas/ProviderModel"
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  },
  post: {
    summary: "Ajoute un nouvel fournisseur",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Fournisseur"],
    parameters: [IdParams],
    requestBody: {
      required: true,
      content: {
        "application/x-www-form-urlencoded": {
          schema: {
            $ref: "#/components/schemas/ProviderModel"
          }
        }
      }
    },
    responses: {
      201: {
        description: "Element bien ajouté",
        content: {
          "application/json": {
            schema: {
              type: "object",
              properties: {
                data: {
                  $ref: "#/components/schemas/UserModel"
                },
                message: {
                  type: "string",
                  example: "OK"
                }
              }
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  },
  put: {
    summary: "Modifie un fournisseur",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Fournisseur"],
    parameters: [IdParams],
    requestBody: {
      required: true,
      content: {
        "application/x-www-form-urlencoded": {
          schema: {
            $ref: "#/components/schemas/ProviderModel"
          }
        }
      }
    },
    responses: {
      200: {
        description: "Element bien modifié",
        content: {
          "application/json": {
            schema: {
              type: "object",
              properties: {
                data: {
                  $ref: "#/components/schemas/ProviderModel"
                },
                message: {
                  type: "string",
                  example: "1 modifié"
                }
              }
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  },
  delete: {
    summary: "Supprime un fournisseur",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Fournisseur"],
    parameters: [IdParams],
    responses: {
      200: {
        description: "Le fournisseur est bien supprimé",
        content: {
          "application/json": {
            schema: {
              type: "object",
              properties: {
                message: {
                  type: "string",
                  example: "1 supprimé"
                }
              }
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  }
};
