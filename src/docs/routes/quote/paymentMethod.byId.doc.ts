import { IdParams } from "../../utils.doc";

export const PaymentMethodById = {
  get: {
    summary: "Récupère un méthode de paiement en fonction de l'id de celui-ci",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Devis"],
    parameters: [IdParams],
    responses: {
      200: {
        description: "Liste un méthode de paiement",
        content: {
          "application/json": {
            schema: {
              $ref: "#/components/schemas/PaymentMethodModel"
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  },
  post: {
    summary: "Ajoute un nouvel méthode de paiement",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Devis"],
    parameters: [IdParams],
    requestBody: {
      required: true,
      content: {
        "application/x-www-form-urlencoded": {
          schema: {
            $ref: "#/components/schemas/PaymentMethodModel"
          }
        }
      }
    },
    responses: {
      201: {
        description: "Element bien ajouté",
        content: {
          "application/json": {
            schema: {
              type: "object",
              properties: {
                data: {
                  $ref: "#/components/schemas/PaymentMethodModel"
                },
                message: {
                  type: "string",
                  example: "OK"
                }
              }
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  },
  put: {
    summary: "Modifie un méthode de paiement",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Devis"],
    parameters: [IdParams],
    requestBody: {
      required: true,
      content: {
        "application/x-www-form-urlencoded": {
          schema: {
            $ref: "#/components/schemas/PaymentMethodModel"
          }
        }
      }
    },
    responses: {
      200: {
        description: "Element bien modifié",
        content: {
          "application/json": {
            schema: {
              type: "object",
              properties: {
                data: {
                  $ref: "#/components/schemas/PaymentMethodModel"
                },
                message: {
                  type: "string",
                  example: "1 modifié"
                }
              }
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  },
  delete: {
    summary: "Supprime un méthode de paiement",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Devis"],
    parameters: [IdParams],
    responses: {
      200: {
        description: "Le méthode de paiement est bien supprimé",
        content: {
          "application/json": {
            schema: {
              type: "object",
              properties: {
                message: {
                  type: "string",
                  example: "1 supprimé"
                }
              }
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  }
};
