import { IdParams } from "../../utils.doc";

export const QuoteById = {
  get: {
    summary: "Récupère un devis en fonction de l'id de celui-ci",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Devis"],
    parameters: [IdParams],
    responses: {
      200: {
        description: "Liste un devis",
        content: {
          "application/json": {
            schema: {
              $ref: "#/components/schemas/QuoteModel"
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  },
  post: {
    summary: "Ajoute un nouvel devis",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Devis"],
    parameters: [IdParams],
    requestBody: {
      required: true,
      content: {
        "application/x-www-form-urlencoded": {
          schema: {
            $ref: "#/components/schemas/QuoteModel"
          }
        }
      }
    },
    responses: {
      201: {
        description: "Element bien ajouté",
        content: {
          "application/json": {
            schema: {
              type: "object",
              properties: {
                data: {
                  $ref: "#/components/schemas/QuoteModel"
                },
                message: {
                  type: "string",
                  example: "OK"
                }
              }
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  },
  put: {
    summary: "Modifie un devis",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Devis"],
    parameters: [IdParams],
    requestBody: {
      required: true,
      content: {
        "application/x-www-form-urlencoded": {
          schema: {
            $ref: "#/components/schemas/QuoteModel"
          }
        }
      }
    },
    responses: {
      200: {
        description: "Element bien modifié",
        content: {
          "application/json": {
            schema: {
              type: "object",
              properties: {
                data: {
                  $ref: "#/components/schemas/QuoteModel"
                },
                message: {
                  type: "string",
                  example: "1 modifié"
                }
              }
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  },
  delete: {
    summary: "Supprime un devis",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Devis"],
    parameters: [IdParams],
    responses: {
      200: {
        description: "Le devis est bien supprimé",
        content: {
          "application/json": {
            schema: {
              type: "object",
              properties: {
                message: {
                  type: "string",
                  example: "1 supprimé"
                }
              }
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  }
};
