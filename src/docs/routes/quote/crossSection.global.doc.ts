export const CrossSectionGlobal = {
  get: {
    summary: "Récupère la liste complète des coupes de principe",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Devis"],
    responses: {
      200: {
        description: "Liste de tous les coupes de principe",
        content: {
          "application/json": {
            schema: {
              type: "array",
              items: {
                $ref: "#/components/schemas/CrossSectionModel"
              }
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  }
};
