export const PaymentMethodGlobal = {
  get: {
    summary: "Récupère la liste complète des méthodes de paiement",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Devis"],
    responses: {
      200: {
        description: "Liste de tous les méthodes de paiement",
        content: {
          "application/json": {
            schema: {
              type: "array",
              items: {
                $ref: "#/components/schemas/PaymentMethodModel"
              }
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  }
};
