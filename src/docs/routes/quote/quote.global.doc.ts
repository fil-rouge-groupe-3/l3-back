export const QuoteGlobal = {
  get: {
    summary: "Récupère la liste complète des devis",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Devis"],
    responses: {
      200: {
        description: "Liste de tous les devis",
        content: {
          "application/json": {
            schema: {
              type: "array",
              items: {
                $ref: "#/components/schemas/QuoteModel"
              }
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  }
};
