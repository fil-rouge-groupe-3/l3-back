export const QuoteStateGlobal = {
  get: {
    summary: "Récupère la liste complète des états de devis",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Devis"],
    responses: {
      200: {
        description: "Liste de tous les états de devis",
        content: {
          "application/json": {
            schema: {
              type: "array",
              items: {
                $ref: "#/components/schemas/QuoteStateModel"
              }
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  }
};
