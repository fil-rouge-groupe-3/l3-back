export const AuthGlobal = {
  post: {
    summary: "Authentifier sur l'application",
    tags: ["Authentification"],
    requestBody: {
      required: true,
      content: {
        "application/x-www-form-urlencoded": {
          schema: {
            type: "object",
            required: ["email", "password"],
            properties: {
              email: {
                type: "string",
                example: "michel.tuffery@oracle.com"
              },
              password: {
                type: "string",
                example: "password"
              }
            }
          }
        }
      }
    },
    responses: {
      201: {
        description: "Utilisateur bien connecté",
        content: {
          "application/json": {
            schema: {
              type: "object",
              properties: {
                access_token: {
                  type: "string",
                  example:
                    "eyJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1NzUyODE2MTIsIm5iZiI6MTU3NTI4MTYxMiwiZXhwIjoxNTc1Mjg1MjEyLCJpc3MiOiJsb2NhbGhvc3QiLCJhdWQiOiJHb1R2U2VyaWVzIiwiaWQiOjU0LCJlbWFpbCI6InRlc3QxQHRlc3QuY29tIiwicHNldWRvIjoidGVzdDEiLCJmaXJzdG5hbWUiOiJUZXN0IiwibGFzdG5hbWUiOiJUZXN0dGVzdCIsInJhbmsiOjN9.eAhK2i4TTPt3GdxQZc3xzridUtAJZrydiSkhfLzH6wY"
                },
                token_type: {
                  type: "string",
                  example: "bearer"
                },
                expire_in: {
                  type: "integer",
                  example: 3600
                }
              }
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      },
      400: {
        description: "Autres erreurs..."
      }
    }
  }
};
