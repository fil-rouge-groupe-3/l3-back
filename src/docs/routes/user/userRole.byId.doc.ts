import { IdParams } from "../../utils.doc";

export const UserRoleById = {
  get: {
    summary: "Récupère un rôle en fonction de l'id de celui-ci",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Utilisateur"],
    parameters: [IdParams],
    responses: {
      200: {
        description: "Liste un role",
        content: {
          "application/json": {
            schema: {
              $ref: "#/components/schemas/UserRoleModel"
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  }
};
