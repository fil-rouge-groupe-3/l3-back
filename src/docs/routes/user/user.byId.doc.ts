import { IdParams } from "../../utils.doc";

export const UserById = {
  get: {
    summary: "Récupère un utilisateur en fonction de l'id de celui-ci",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Utilisateur"],
    parameters: [IdParams],
    responses: {
      200: {
        description: "Liste un utilisateur",
        content: {
          "application/json": {
            schema: {
              $ref: "#/components/schemas/UserModel"
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  },
  post: {
    summary: "Ajoute un nouvel utilisateur",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Utilisateur"],
    parameters: [IdParams],
    requestBody: {
      required: true,
      content: {
        "application/x-www-form-urlencoded": {
          schema: {
            $ref: "#/components/schemas/UserModel"
          }
        }
      }
    },
    responses: {
      201: {
        description: "Element bien ajouté",
        content: {
          "application/json": {
            schema: {
              type: "object",
              properties: {
                data: {
                  $ref: "#/components/schemas/UserModel"
                },
                message: {
                  type: "string",
                  example: "OK"
                }
              }
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  },
  put: {
    summary: "Modifie un utilisateur",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Utilisateur"],
    parameters: [IdParams],
    requestBody: {
      required: true,
      content: {
        "application/x-www-form-urlencoded": {
          schema: {
            $ref: "#/components/schemas/UserModel"
          }
        }
      }
    },
    responses: {
      200: {
        description: "Element bien modifié",
        content: {
          "application/json": {
            schema: {
              type: "object",
              properties: {
                data: {
                  $ref: "#/components/schemas/UserModel"
                },
                message: {
                  type: "string",
                  example: "1 modifié"
                }
              }
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  },
  delete: {
    summary: "Supprime un utilisateur",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Utilisateur"],
    parameters: [IdParams],
    responses: {
      200: {
        description: "L'utilisateur est bien supprimé",
        content: {
          "application/json": {
            schema: {
              type: "object",
              properties: {
                message: {
                  type: "string",
                  example: "1 supprimé"
                }
              }
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  }
};
