export const UserRoleGlobal = {
  get: {
    summary: "Récupère la liste complète des rôles utilisateurs",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Utilisateur"],
    responses: {
      200: {
        description: "Liste de tous les rôles utilisateurs",
        content: {
          "application/json": {
            schema: {
              type: "array",
              items: {
                $ref: "#/components/schemas/UserRoleModel"
              }
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  }
};
