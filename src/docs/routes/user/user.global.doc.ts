export const UserGlobal = {
  get: {
    summary: "Récupère la liste complète des utilisateurs",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Utilisateur"],
    responses: {
      200: {
        description: "Liste de tous les utilisateurs",
        content: {
          "application/json": {
            schema: {
              type: "array",
              items: {
                $ref: "#/components/schemas/UserModel"
              }
            }
          }
        }
      },
      409: {
        description: "Utilisateur non trouvé"
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  }
};

export const UserCurrent = {
  get: {
    summary: "Récupère les information de l'utilisateur courant",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Utilisateur"],
    responses: {
      200: {
        description: "Liste les informations de l'utilisateur courant",
        content: {
          "application/json": {
            schema: {
              $ref: "#/components/schemas/UserModel"
            }
          }
        }
      },
      409: {
        description: "Utilisateur non trouvé"
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  }
};
