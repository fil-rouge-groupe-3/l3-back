export const CustomerGlobal = {
  get: {
    summary: "Récupère la liste complète des clients",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Client"],
    responses: {
      200: {
        description: "Liste de tous les clients",
        content: {
          "application/json": {
            schema: {
              type: "array",
              items: {
                $ref: "#/components/schemas/CustomerModel"
              }
            }
          }
        }
      },
      409: {
        description: "Client non trouvé"
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  }
};
