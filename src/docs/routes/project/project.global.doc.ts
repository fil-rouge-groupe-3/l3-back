export const ProjectGlobal = {
  get: {
    summary: "Récupère la liste complète des projets",
    security: [
      {
        bearer: []
      }
    ],
    tags: ["Projet"],
    responses: {
      200: {
        description: "Liste de tous les projets",
        content: {
          "application/json": {
            schema: {
              type: "array",
              items: {
                $ref: "#/components/schemas/ProjectModel"
              }
            }
          }
        }
      },
      401: {
        description: "Access token n'existe pas ou est invalide"
      }
    }
  }
};
