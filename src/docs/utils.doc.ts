export const HeaderBearer = {
  in: "header",
  name: "Authorization",
  schema: {
    type: "string",
    format: "uuid"
  },
  required: true,
  description: "Token JWT fourni lors de l'authentification"
};

export const IdParams = {
  in: "path",
  name: "id",
  schema: {
    type: "integer"
  },
  required: true,
  description: "Id de l'élément auquel accéder"
};
