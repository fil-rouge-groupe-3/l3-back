export const QuoteModel = {
  type: "object",
  required: [
    "name",
    "amoutHt",
    "crossSection",
    "paymentMethod",
    "quoteState",
    "projects",
    "orders",
    "createdAt"
  ],
  properties: {
    id: {
      type: "integer",
      example: 1
    },
    name: {
      type: "string",
      example: "Controler les bases du monde"
    },
    amoutHt: {
      type: "integer",
      example: "55"
    },
    crossSection: {
      $ref: "#/components/schemas/CrossSectionModel"
    },
    paymentMethod: {
      $ref: "#/components/schemas/PaymentMethodModel"
    },
    quoteState: {
      $ref: "#/components/schemas/QuoteStateModel"
    },
    projects: {
      type: "object"
    },
    orders: {
      type: "object"
    },
    createdAt: {
      type: "string",
      example: "01/20/2020"
    },
    updatedAt: {
      type: "string",
      example: "01/20/2020"
    }
  }
};
