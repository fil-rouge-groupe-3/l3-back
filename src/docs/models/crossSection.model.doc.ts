export const CrossSectionModel = {
  type: "object",
  required: ["blueprintName", "blueprintPath"],
  properties: {
    id: {
      type: "integer",
      example: 1
    },
    blueprintName: {
      type: "string",
      example: "Plan d'Oracle"
    },
    blueprintPath: {
      type: "string",
      example: "//path/path/le/loup"
    },
    quotes: {
      type: "array",
      items: {
        $ref: "#/components/schemas/QuoteModel"
      }
    }
  }
};
