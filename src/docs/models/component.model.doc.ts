export const ComponentModel = {
  type: "object",
  required: [
    "elementUsageUnit",
    "componentGroup",
    "elementCharacteristic",
    "elementNature",
    "provider",
    "orders",
    "priceHt"
  ],
  properties: {
    id: {
      type: "integer",
      example: 1
    },
    elementUsageUnit: {
      $ref: "#/components/schemas/ElementUsageUnitModel"
    },
    componentGroup: {
      $ref: "#/components/schemas/ComponentGroupModel"
    },
    elementCharacteristic: {
      $ref: "#/components/schemas/ElementCharacteristicModel"
    },
    elementNature: {
      $ref: "#/components/schemas/ElementNatureModel"
    },
    provider: {
      $ref: "#/components/schemas/ProviderModel"
    },
    orders: {
      type: "array",
      items: {
        type: "object"
      }
    },
    priceHt: {
      type: "integer",
      example: 155
    }
  }
};
