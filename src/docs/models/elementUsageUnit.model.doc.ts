export const ElementUsageUnitModel = {
  type: "object",
  required: ["name"],
  properties: {
    id: {
      type: "integer",
      example: 1
    },
    name: {
      type: "string",
      example: "M linéaire"
    }
  }
};
