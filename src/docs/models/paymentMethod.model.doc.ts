export const PaymentMethodModel = {
  type: "object",
  required: ["stepName", "percentageToUnlock", "crossSection"],
  properties: {
    id: {
      type: "integer",
      example: 1
    },
    stepName: {
      type: "string",
      example: "Etape 1"
    },
    description: {
      type: "string",
      example: "Super Etape 1"
    },
    percentageToUnlock: {
      type: "integer",
      example: "15"
    },
    quotes: {
      type: "array",
      items: {
        $ref: "#/components/schemas/QuoteModel"
      }
    }
  }
};
