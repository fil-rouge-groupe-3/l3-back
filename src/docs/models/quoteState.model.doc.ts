export const QuoteStateModel = {
  type: "object",
  required: ["name", "color"],
  properties: {
    id: {
      type: "integer",
      example: 1
    },
    name: {
      type: "string",
      example: "Oracle"
    },
    description: {
      type: "string",
      example: "Super Oracle"
    },
    color: {
      type: "string",
      example: "#FFF"
    },
    quotes: {
      type: "array",
      items: {
        $ref: "#/components/schemas/QuoteModel"
      }
    }
  }
};
