export const ProjectModel = {
  type: "object",
  required: [
    "ref",
    "name",
    "desc",
    "users",
    "customers",
    "quotes",
    "updatedAt",
    "startedAt",
    "endedAt"
  ],
  properties: {
    id: {
      type: "integer",
      example: 1
    },
    ref: {
      type: "integer",
      example: "6251321546"
    },
    name: {
      type: "string",
      example: "Projet super"
    },
    desc: {
      type: "string",
      example: "Super Projet"
    },
    users: {
      type: "array",
      items: {
        $ref: "#/components/schemas/UserModel"
      }
    },
    customers: {
      type: "array",
      items: {
        $ref: "#/components/schemas/CustomerModel"
      }
    },
    quotes: {
      type: "array",
      items: {
        $ref: "#/components/schemas/QuoteModel"
      }
    },
    createdAt: {
      type: "string",
      example: "01/20/2020"
    },
    updatedAt: {
      type: "string",
      example: "01/20/2020"
    },
    startedAt: {
      type: "string",
      example: "01/20/2020"
    },
    endedAt: {
      type: "string",
      example: "01/20/2020"
    }
  }
};
