export const ProviderModel = {
  type: "object",
  required: [
    "email",
    "name",
    "zipcode",
    "address",
    "city",
    "tel",
    "website",
    "country",
    "siret"
  ],
  properties: {
    id: {
      type: "integer",
      example: 1
    },
    name: {
      type: "string",
      example: "MichelCorp"
    },
    email: {
      type: "string",
      example: "michel.tuffery@oracle.com"
    },
    zipcode: {
      type: "string",
      example: "31400"
    },
    address: {
      type: "string",
      example: "42 rue de L'Oracle du SQL"
    },
    city: {
      type: "string",
      example: "OracleVille"
    },
    tel: {
      type: "string",
      example: "0505050505"
    },
    website: {
      type: "string",
      example: "https://michel-tuffery.com"
    },
    country: {
      type: "string",
      example: "France"
    },
    siret: {
      type: "string",
      example: "6462154946165449"
    }
  }
};
