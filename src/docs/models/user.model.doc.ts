export const UserModel = {
  type: "object",
  required: ["email", "password"],
  properties: {
    id: {
      type: "integer",
      example: 1
    },
    firstName: {
      type: "string",
      example: "Michel"
    },
    lastName: {
      type: "string",
      example: "Tuffery"
    },
    email: {
      type: "string",
      example: "michel.tuffery@oracle.com"
    },
    password: {
      type: "string",
      example: "password"
    },
    zipcode: {
      type: "string",
      example: "31400"
    },
    address: {
      type: "string",
      example: "42 rue de L'Oracle du SQL"
    },
    city: {
      type: "string",
      example: "OracleVille"
    },
    tel: {
      type: "string",
      example: "0505050505"
    },
    avatar_img: {
      type: "string",
      example: "./photo_de_vacances.png"
    },
    userRole: {
      $ref: "#/components/schemas/UserRoleModel"
    },
    projects: {
      $ref: "#/components/schemas/ProjectModel"
    }
  }
};
