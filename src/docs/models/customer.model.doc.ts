export const CustomerModel = {
  type: "object",
  required: [
    "firstName",
    "lastName",
    "company",
    "siret",
    "address",
    "city",
    "zipcode",
    "tel"
  ],
  properties: {
    id: {
      type: "integer",
      example: 1
    },
    firstName: {
      type: "string",
      example: "Michel"
    },
    lastName: {
      type: "string",
      example: "Tuffery"
    },
    company: {
      type: "string",
      example: "Michel Corp"
    },
    siret: {
      type: "string",
      example: "65461164"
    },
    address: {
      type: "string",
      example: "42 rue de L'Oracle du SQL"
    },
    city: {
      type: "string",
      example: "OracleVille"
    },
    zipcode: {
      type: "string",
      example: "31400"
    },
    tel: {
      type: "string",
      example: "0505050505"
    },
    projects: {
      $ref: "#/components/schemas/ProjectModel"
    }
  }
};
