export const UserRoleModel = {
  type: "object",
  properties: {
    id: {
      type: "integer",
      example: 1
    },
    name: {
      type: "string",
      example: "ADMIN"
    },
    description: {
      type: "string",
      example: "Administrateur"
    }
  }
};
