import { Component } from "../database/entities/components/component.entity";
import { BaseController } from "./base.controller";

export default class ComponentController extends BaseController<Component> {
  constructor() {
    super(Component);
  }
}
