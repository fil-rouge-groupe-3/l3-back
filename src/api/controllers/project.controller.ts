import { Project } from "../database/entities/project.entity";
import { BaseController } from "./base.controller";
import { getRepository } from "typeorm";
import { Request, Response } from "express";
import { validate } from "class-validator";

export default class ProjectController extends BaseController<Project> {
  constructor() {
    super(Project);
  }

  async putCustom(req: Request, res: Response) {
    const id = req.params.id;
    const repository = getRepository(Project);
    try {
      await repository.findOneOrFail(id);
    } catch (error) {
      //If not found, send a 404 response
      res.status(404).json({
        message: `L'id : (${req.params.id}) non trouvé`
      });
      return;
    }

    let data: Project;
    data = req.body;

    const errors = await validate(data);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }

    try {
      await repository.update(id, {
        id: Number(id),
        name: data.name,
        desc: data.desc,
        ref: data.ref,
        updatedAt: data.updatedAt,
        startedAt: data.startedAt,
        endedAt: data.endedAt
      });
    } catch (e) {
      res.status(409).json({
        message: "Element déjà utilisé",
        detail: e.message
      });
      return;
    }

    try {
      await repository.save({
        id: Number(id),
        users: data.users,
        customers: data.customers
      });
    } catch (e) {
      res.status(409).json({
        message: "Element déjà utilisé",
        detail: e.message
      });
      return;
    }

    res.status(200).json({
      data,
      message: `${req.params.id} modifié`
    });
  }
}
