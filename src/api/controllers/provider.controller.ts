import { BaseController } from "./base.controller";
import { Provider } from "../database/entities/provider.entity";

export default class ProviderController extends BaseController<Provider> {
  constructor() {
    super(Provider);
  }
}
