import { BaseController } from "./base.controller";
import { Customer } from "../database/entities/customer.entity";

export default class CustomerController extends BaseController<Customer> {
  constructor() {
    super(Customer);
  }
}
