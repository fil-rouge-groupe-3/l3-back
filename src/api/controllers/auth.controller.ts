import { Request, Response } from "express";
import { User } from "../database/entities/user.entity";
import { validate } from "class-validator";
import { getRepository } from "typeorm";
import jwt = require("jsonwebtoken");
import bcrypt = require("bcryptjs");
import { IPayload } from "../models/payload.model";

export default class AuthController {
  async login(req: Request, res: Response) {
    let data: User, expire_in: number, user: User;

    if (!req.body.email && !req.body.password) {
      res.status(400).send("The fields are empty");
      return;
    }

    data = req.body;
    expire_in = 1440;

    //Validate if the parameters are ok
    const errors = await validate(data);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }

    //Try to save. If fails, the username is already in use
    const repository = getRepository(User);
    user = await repository.findOne({
      where: { email: data.email },
      relations: ["userRole"],
      select: ["id", "password", "email", "avatar_img"]
    });

    if (user != undefined) {
      bcrypt.compare(data.password, user.password).then(isMatch => {
        if (!isMatch) {
          res.status(401).send("Incorrect Password");
          return;
        }
        // Create Token
        const payload: IPayload = {
          id: user.id,
          email: user.email,
          avatar: user.avatar_img,
          rank: user.userRole.id
        };

        let token = jwt.sign(payload, process.env.JWT_SECRET as string, {
          expiresIn: expire_in
        });

        //If all ok, send 201 response
        res.status(201).json({
          access_token: token,
          token_type: "bearer",
          expire_in: expire_in
        });
      });
    } else {
      res.status(409).json({
        message: "User not found"
      });
    }
  }

  async register(req: Request, res: Response) {
    let user: User, data: User, expire_in: number;

    if (!req.body) {
      res.status(400).send("The fields are empty");
      return;
    }

    //Get parameters from the body
    expire_in = 1440;
    data = req.body;

    //Validate if the parameters are ok
    const errors = await validate(data);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }
    let salt = bcrypt.genSaltSync(12);
    data.password = bcrypt.hashSync(data.password, salt);

    //Try to save. If fails, the username is already in use
    const repository = getRepository(User);
    try {
      user = await repository.save(data);
    } catch (e) {
      res.status(409).send("Already in use");
      return;
    }

    // Create Token
    const payload: IPayload = {
      id: user.id,
      email: user.email,
      avatar: user.avatar_img,
      rank: user.userRole.id
    };

    let token = jwt.sign(payload, process.env.JWT_SECRET as string, {
      expiresIn: expire_in
    });

    //If all ok, send 201 response
    res.status(201).json({
      access_token: token,
      token_type: "bearer",
      expire_in: expire_in
    });
  }

  async forgotPassword(req: Request, res: Response) {
    let data: {
      email: string;
      password: string;
    };

    if (!req.body) {
      res.status(400).send("The fields are empty");
      return;
    }

    data = {
      email: req.body.email,
      password: req.body.newPassword
    };

    let salt = bcrypt.genSaltSync(12);
    data.password = bcrypt.hashSync(data.password, salt);

    //Try to save. If fails, the username is already in use
    const repository = getRepository(User);
    await repository
      .createQueryBuilder()
      .update()
      .set({ password: data.password })
      .where("user.email = :email", { email: data.email })
      .execute()
      .catch(e => {
        res.status(409).json({
          message: "User not found",
          error: e
        });
        return;
      });

    //If all ok, send 201 response
    res.status(201).json({
      message: "Password changed !"
    });
  }
}
