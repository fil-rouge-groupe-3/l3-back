import { BaseController } from "./base.controller";
import { ComponentGroup } from "../database/entities/components/componentGroup.entity";

export default class ComponentGroupController extends BaseController<
  ComponentGroup
> {
  constructor() {
    super(ComponentGroup);
  }
}
