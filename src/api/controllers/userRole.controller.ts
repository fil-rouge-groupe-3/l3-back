import { BaseController } from "./base.controller";
import { UserRole } from "../database/entities/userRole.entity";

export class UserRoleController extends BaseController<UserRole> {
  constructor() {
    super(UserRole);
  }
}
