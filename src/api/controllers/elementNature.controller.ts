import { BaseController } from "./base.controller";
import { ElementNature } from "../database/entities/elementNature.entity";

export default class ElementNatureController extends BaseController<
  ElementNature
> {
  constructor() {
    super(ElementNature);
  }
}
