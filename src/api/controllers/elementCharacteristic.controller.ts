import { BaseController } from "./base.controller";
import { ElementCharacteristic } from "../database/entities/elementCharacteristic.entity";

export default class ElementCharacteristicController extends BaseController<
  ElementCharacteristic
> {
  constructor() {
    super(ElementCharacteristic);
  }
}
