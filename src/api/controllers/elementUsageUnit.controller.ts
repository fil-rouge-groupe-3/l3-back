import { BaseController } from "./base.controller";
import { ElementUsageUnit } from "../database/entities/elementUsageUnit.entity";

export default class ElementUsageUnitController extends BaseController<
  ElementUsageUnit
> {
  constructor() {
    super(ElementUsageUnit);
  }
}
