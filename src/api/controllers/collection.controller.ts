import { Collection } from "../database/entities/collection.entity";
import { BaseController } from "./base.controller";

export default class CollectionController extends BaseController<Collection> {
  constructor() {
    super(Collection);
  }
}
