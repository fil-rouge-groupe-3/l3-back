import { Module } from "../database/entities/modules/module.entity";
import { BaseController } from "./base.controller";

export default class ModuleController extends BaseController<Module> {
  constructor() {
    super(Module);
  }
}
