import { BaseController } from "./base.controller";
import { Order } from "../database/entities/order.entity";

export default class OrderController extends BaseController<Order> {
  constructor() {
    super(Order);
  }
}
