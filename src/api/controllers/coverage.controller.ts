import { BaseController } from "./base.controller";
import { Coverage } from "../database/entities/coverage.entity";

export default class CoverageController extends BaseController<Coverage> {
  constructor() {
    super(Coverage);
  }
}
