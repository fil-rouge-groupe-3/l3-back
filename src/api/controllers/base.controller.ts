import { Request, Response } from "express";
import { getRepository, EntitySchema } from "typeorm";
import { validate } from "class-validator";

/**
 * Class générique gérant la logique des méthodes de base du controller
 *
 * @export
 * @class BaseController
 * @template T
 */
export class BaseController<T> {
  /**
   *Creates an instance of BaseController.
   * @param {new () => T} type Nom de l'entité
   * @memberof BaseController
   */
  constructor(private type: new () => T) {}

  /**
   * Récupérer l'ensemble des éléments de la base de données.
   *
   * @template T Type générique du model
   * @param {Request} req Requête d'entrée par l'appel de la route
   * @param {Response} res Réponse fournit par le serveur web
   * @param {(T | EntitySchema<T>)} repo Nom de l'entité
   * @param {([] | string[])} arraySelect Filtre pour les champs du json
   * @param {([] | string[])} [arrayRelation=[]] Filtre pour les relations entre Entité
   * @memberof BaseController
   *
   * @example
   *    userController.getAll(req, res, User, ["id", "email"], ["role"])
   */
  async getAll<T>(
    _req: Request,
    res: Response,
    repo: T | EntitySchema<T>,
    arraySelect: [] | string[],
    arrayRelation: [] | string[] = []
  ) {
    const repository = getRepository(repo as EntitySchema<T>);
    const data = await repository.find({
      select: arraySelect as [],
      relations: arrayRelation as []
    });
    res.json(data);
  }

  /**
   * Ajouter un élément dans la base de données.
   *
   * @template T Type générique du model
   * @param {Request} req Requête d'entrée par l'appel de la route
   * @param {Response} res Réponse fournit par le serveur web
   * @param {(T | EntitySchema<T>)} repo Nom de l'entité
   * @memberof BaseController
   */
  async post<T>(req: Request, res: Response, repo: T | EntitySchema<T>) {
    //Get parameters from the body
    let data = new this.type();
    data = req.body;

    //Validade if the parameters are ok
    const errors = await validate(data);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }

    //Try to save. If fails, the username is already in use
    const repository = getRepository(repo as EntitySchema<T>);
    try {
      await repository.save(data);
    } catch (e) {
      res.status(409).send("Already in use");
      return;
    }

    //If all ok, send 201 response
    res.status(201).json({
      data,
      message: "OK"
    });
  }

  /**
   * Récupérer un élément de la base de données.
   *
   * @template T Type générique du model
   * @param {Request} req Requête d'entrée par l'appel de la route
   * @param {Response} res Réponse fournit par le serveur web
   * @param {(T | EntitySchema<T>)} repo Nom de l'entité
   * @param {([] | string[])} arraySelect Filtre pour les champs du json
   * @param {([] | string[])} [arrayRelation=[]] Filtre pour les relations entre Entité
   * @memberof BaseController
   */
  async getOneById<T>(
    req: Request,
    res: Response,
    repo: T | EntitySchema<T>,
    arraySelect: [] | string[],
    arrayRelation: [] | string[] = []
  ) {
    //Get the ID from the url
    const id = req.params.id;
    //Get the user from database
    const repository = getRepository(repo as EntitySchema<T>);
    try {
      const data = await repository.findOneOrFail(id, {
        select: arraySelect as [], //We dont want to send the password on response
        relations: arrayRelation as []
      });
      res.send(data);
    } catch (error) {
      res.status(404).json({
        message: `L'id : (${req.params.id}) non trouvé`
      });
    }
  }

  /**
   * Modifier un élément dans la base de données.
   *
   * @template T Type générique du model
   * @param {Request} req Requête d'entrée par l'appel de la route
   * @param {Response} res Réponse fournit par le serveur web
   * @param {(T | EntitySchema<T>)} repo Nom de l'entité
   * @memberof BaseController
   */
  async put<T>(req: Request, res: Response, repo: T | EntitySchema<T>) {
    //Get the ID from the url
    const id = req.params.id;

    //Try to find user on database
    const repository = getRepository(repo as EntitySchema<T>);
    try {
      await repository.findOneOrFail(id);
    } catch (error) {
      //If not found, send a 404 response
      res.status(404).json({
        message: `L'id : (${req.params.id}) non trouvé`
      });
      return;
    }

    //Validate the new values on model
    let data = new this.type();
    data = req.body;

    const errors = await validate(data);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }

    //Try to safe, if fails, that means username already in use
    try {
      await repository.update(id, data);
    } catch (e) {
      res.status(409).json({
        message: "Element déjà utilisé"
      });
      return;
    }
    //After all send a 204 (no content, but accepted) response
    res.status(200).json({
      data,
      message: `${req.params.id} modifié`
    });
  }

  /**
   * Supprimer un élément de la base de données.
   *
   * @template T Type générique du model
   * @param {Request} req Requête d'entrée par l'appel de la route
   * @param {Response} res Réponse fournit par le serveur web
   * @param {(T | EntitySchema<T>)} repo Nom de l'entité
   * @memberof BaseController
   */
  async delete<T>(req: Request, res: Response, repo: T | EntitySchema<T>) {
    //Get the ID from the url
    const id = req.params.id;

    const repository = getRepository(repo as EntitySchema<T>);
    try {
      await repository.findOneOrFail(id);
    } catch (error) {
      res.status(404).json({
        message: `L'id : (${req.params.id}) non trouvé`
      });
      return;
    }
    repository.delete(id);

    //After all send a 204 (no content, but accepted) response
    res.status(200).json({
      message: `${req.params.id} supprimé`
    });
  }
}
