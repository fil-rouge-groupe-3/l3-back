import { Cctp } from "../database/entities/cctp.entity";
import { BaseController } from "./base.controller";

export default class CctpController extends BaseController<Cctp> {
  constructor() {
    super(Cctp);
  }
}
