import { PaymentMethod } from "../database/entities/paymentMethod.entity";
import { BaseController } from "./base.controller";

export default class PaymentMethodController extends BaseController<PaymentMethod> {
  constructor() {
    super(PaymentMethod);
  }
}
