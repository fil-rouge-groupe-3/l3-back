import { BaseController } from "./base.controller";
import { CrossSection } from "../database/entities/crossSection.entity";

export default class CrossSectionController extends BaseController<
  CrossSection
> {
  constructor() {
    super(CrossSection);
  }
}
