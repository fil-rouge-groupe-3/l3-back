import { BaseController } from "./base.controller";
import { DoorFrame } from "../database/entities/doorFrame.entity";

export default class DoorFrameController extends BaseController<DoorFrame> {
  constructor() {
    super(DoorFrame);
  }
}
