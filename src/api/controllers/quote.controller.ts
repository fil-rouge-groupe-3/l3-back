import { BaseController } from "./base.controller";
import { Quote } from "../database/entities/quote.entity";

export default class QuoteController extends BaseController<Quote> {
  constructor() {
    super(Quote);
  }
}
