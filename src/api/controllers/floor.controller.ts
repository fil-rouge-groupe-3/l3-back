import { BaseController } from "./base.controller";
import { Floor } from "../database/entities/floor.entity";

export default class FloorController extends BaseController<Floor> {
  constructor() {
    super(Floor);
  }
}
