import { BaseController } from "./base.controller";
import { QuoteState } from "../database/entities/quoteState.entity";

export default class QuoteStateController extends BaseController<QuoteState> {
  constructor() {
    super(QuoteState);
  }
}
