import { User } from "../database/entities/user.entity";
import { BaseController } from "./base.controller";
import { Response, Request } from "express";
import jwt = require("jsonwebtoken");
import { getRepository } from "typeorm";
import { IPayload } from "../models/payload.model";

export default class UserController extends BaseController<User> {
  constructor() {
    super(User);
  }

  async getCurrentUser(
    req: Request,
    res: Response,
    arraySelect: [] | string[],
    arrayRelation: [] | string[] = []
  ) {
    let token: string = req.headers["authorization"];
    if (token) {
      token = token.replace("Bearer ", "");
      jwt.verify(
        token,
        process.env.JWT_SECRET as string,
        async (err, decoded: IPayload) => {
          if (err) {
            return res.status(401).json({ message: "invalid_token" });
          } else {
            try {
              const data = await getRepository(User).findOneOrFail(
                (decoded as IPayload).id as number,
                {
                  select: arraySelect as [],
                  relations: arrayRelation as []
                }
              );
              res.send(data);
            } catch (error) {
              res.status(404).send({
                message: `L'id : (${req.params.id}) non trouvé`
              });
            }
          }
        }
      );
    } else {
      res.status(401).send({ message: "no_token_provided" });
    }
  }
}
