import { CrossSection } from "../entities/crossSection.entity";

export const CrossSectionSeed: CrossSection[] = [
  {
    id: 1,
    blueprintName: "DOMAINE DE CHANTILLY - COYE LA FORET",
    blueprintPath: "",
    quotes: []
  },
  {
    id: 2,
    blueprintName: "Extension de quai",
    blueprintPath: "",
    quotes: []
  },
  {
    id: 3,
    blueprintName: "Maison basse consommation",
    blueprintPath: "",
    quotes: []
  },
  {
    id: 4,
    blueprintName: "Coupe du jardin A",
    blueprintPath: "",
    quotes: []
  },
  {
    id: 5,
    blueprintName: "Fondation basse consommation",
    blueprintPath: "",
    quotes: []
  },
  {
    id: 6,
    blueprintName: "Coupe du jardin B",
    blueprintPath: "",
    quotes: []
  },
  {
    id: 7,
    blueprintName: "Coupe du jardin C",
    blueprintPath: "",
    quotes: []
  },
  {
    id: 8,
    blueprintName: "Grand garage économe",
    blueprintPath: "",
    quotes: []
  },
  {
    id: 9,
    blueprintName: "Jardin filtrant",
    blueprintPath: "",
    quotes: []
  },
  {
    id: 10,
    blueprintName: "Coupe de principe Abris de jardin",
    blueprintPath: "",
    quotes: []
  }
];
