import { ModuleSection } from "../entities/modules/moduleSection.entity";

export const ModuleSectionSeed: ModuleSection[] = [
  {
    id: 1,
    name: "Lisses"
  },
  {
    id: 2,
    name: "Contreforts"
  },
  {
    id: 3,
    name: "Sabots d’assemblage"
  },
  {
    id: 4,
    name: "Goujons de fixation"
  },
  {
    id: 5,
    name: "Supports de sol"
  }
];
