import { ModuleRising } from "../entities/modules/moduleRising.entity";

export const ModuleRisingSeed: ModuleRising[] = [
  {
    id: 1,
    name: "Départ – Sabots d’assemblage … "
  },
  {
    id: 2,
    name: "Arrivée – Sabots d’assemblage … "
  },
  {
    id: 3,
    name: "Intermédiaire – Sabots d’assemblage … "
  }
];
