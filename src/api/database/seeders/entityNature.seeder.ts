import { ElementNature } from "../entities/elementNature.entity";

export const ElementNatureSeed: ElementNature[] = [
  {
    id: 1,
    name: "Montants en bois pour la structure, nommés lisses ou contrefort"
  },
  {
    id: 2,
    name: "Eléments de montages, sabots métalliques, boulons, gougeons"
  },
  {
    id: 3,
    name: "Panneaux d’isolation et pare-pluie"
  },
  {
    id: 4,
    name: "Panneaux intermédiaires et de couverture (intérieur ou extérieur)"
  },
  {
    id: 5,
    name: "Planchers"
  },
  {
    id: 6,
    name: "Couverture (tuiles ou ardoises)"
  },
  {
    id: 7,
    name: "Murs extérieurs"
  },
  {
    id: 8,
    name: "Cloisons intérieures"
  },
  {
    id: 9,
    name: "Plancher sur dalle"
  },
  {
    id: 10,
    name: "Plancher porteur"
  },
  {
    id: 11,
    name: "Fermes de charpente"
  },
  {
    id: 12,
    name: "Couverture (toit)"
  }
];
