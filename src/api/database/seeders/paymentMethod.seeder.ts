import { PaymentMethod } from "../entities/paymentMethod.entity";

export const PaymentMethodSeed: PaymentMethod[] = [
  {
    id: 1,
    stepName: "A la signature",
    description: "",
    percentageToUnlock: 3,
    quotes: []
  },
  {
    id: 2,
    stepName: "Obtention du permis de construire",
    description: "",
    percentageToUnlock: 10,
    quotes: []
  },
  {
    id: 3,
    stepName: "Ouverture du chantier",
    description: "",
    percentageToUnlock: 15,
    quotes: []
  },
  {
    id: 4,
    stepName: "Achèvement des fondations",
    description: "",
    percentageToUnlock: 25,
    quotes: []
  },
  {
    id: 5,
    stepName: "Achèvement des murs",
    description: "",
    percentageToUnlock: 40,
    quotes: []
  },
  {
    id: 6,
    stepName: "Mise hors d’eau/hors d’air",
    description: "",
    percentageToUnlock: 75,
    quotes: []
  },
  {
    id: 7,
    stepName:
      "Achèvement des travaux d’équipement (plomberie, menuiserie, chauffage)",
    description: "",
    percentageToUnlock: 95,
    quotes: []
  },
  {
    id: 8,
    stepName: "Remise des clés",
    description: "",
    percentageToUnlock: 100,
    quotes: []
  }
];
