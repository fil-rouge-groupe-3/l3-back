import { ComponentGroup } from "../entities/components/componentGroup.entity";

export const ComponentGroupSeed: ComponentGroup[] = [
  {
    id: 1,
    name: "Isolant",
    description: ""
  },
  {
    id: 2,
    name: "Fondation",
    description: ""
  },
  {
    id: 3,
    name: "Façade",
    description: ""
  },
  {
    id: 4,
    name: "Sol",
    description: ""
  },
  {
    id: 5,
    name: "Toiture",
    description: ""
  },
  {
    id: 6,
    name: "Couverture",
    description: ""
  },
  {
    id: 7,
    name: "Charpente",
    description: ""
  }
];
