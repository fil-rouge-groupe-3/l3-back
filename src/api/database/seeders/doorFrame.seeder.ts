import { DoorFrame } from "../entities/doorFrame.entity";

export const DoorFrameSeed: DoorFrame[] = [
  {
    id: 1,
    name: "Bois blanc",
    quality: "Bois"
  },
  {
    id: 2,
    name: "Bois noir",
    quality: "Bois"
  },
  {
    id: 3,
    name: "Aluminium blanc",
    quality: "Aluminium"
  },
  {
    id: 4,
    name: "Aluminium noir",
    quality: "Aluminium"
  },
  {
    id: 5,
    name: "PVC",
    quality: "PVC"
  }
];
