import { ElementUsageUnit } from "../entities/elementUsageUnit.entity";

export const ElementUsageUnitSeed: ElementUsageUnit[] = [
  {
    id: 1,
    name: "M linéaire"
  },
  {
    id: 2,
    name: "M 2"
  },
  {
    id: 3,
    name: "Unité"
  },
  {
    id: 4,
    name: "Longeur en cm"
  },
  {
    id: 5,
    name: "Pièce"
  },
  {
    id: 6,
    name: "Surface en M2"
  }
];
