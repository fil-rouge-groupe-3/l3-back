import { Cctp } from "../entities/cctp.entity";

export const CctpSeed: Cctp[] = [
    {
        id: 1,
        name: "Filtres plantes de roseaux"
    },
    {
        id: 2,
        name: "Electricité"
    },
    {
        id: 3,
        name: "Espace vert - Mobiler Urbain"
    },
    {
        id: 4,
        name: "Lot 1 - Gros oeuvre"
    },
    {
        id: 5,
        name: "Electricité Courant fort/ courant faible"
    },
    {
        id: 6,
        name: "Travaux de voirie"
    },
    {
        id: 7,
        name: "Lot 2 - Gros oeuvre"
    },
];