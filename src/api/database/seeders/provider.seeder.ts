import * as faker from "faker";
import { Provider } from "../entities/provider.entity";

export const getProviderSeeder = (number: number) => {
  let providers: Provider[] = [];

  for (let i = 1; i < number; i++) {
    let provider = new Provider();
    provider.id = i;
    provider.name = faker.company.companyName();
    provider.email = faker.internet.email(
      "contact",
      "com",
      `${provider.name
        .toLowerCase()
        .replace(/\s/g, "")
        .replace(",", "")}.com`
    );
    provider.tel = faker.phone.phoneNumber("06########");
    provider.website = `https://${provider.name
      .toLowerCase()
      .replace(/\s/g, "")
      .replace(",", "")}.com`;
    provider.zipcode = faker.address.zipCode();
    provider.city = faker.address.city();
    provider.country = faker.address.country();
    provider.address = `${faker.random.number(
      100
    )} ${faker.address.streetName()}`;
    provider.siret = faker.random.number(99999999999999).toString();

    providers.push(provider);
  }

  return providers;
};
