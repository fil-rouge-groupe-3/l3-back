import { ElementCharacteristic } from "../entities/elementCharacteristic.entity";

export const ElementCharacteristicSeed: ElementCharacteristic[] = [
  {
    id: 1,
    name: "Section en cm "
  },
  {
    id: 2,
    name: "Epaisseur en cm"
  },
  {
    id: 3,
    name: "Epaisseur en mm"
  },
  {
    id: 4,
    name: "Longueur et largeur en mm"
  },
  {
    id: 5,
    name: "Hauteur-Longueur"
  },
  {
    id: 6,
    name: "Longueur"
  }
];
