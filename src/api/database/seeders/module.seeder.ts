import { Module } from "../entities/modules/module.entity";
import { Cctp } from "../entities/cctp.entity";
import { ModuleFillingBetweenRising } from "../entities/modules/moduleFillingBetweenRising.entity";
import { ModuleRising } from "../entities/modules/moduleRising.entity";
import { ModuleSection } from "../entities/modules/moduleSection.entity";
import { ElementCharacteristic } from "../entities/elementCharacteristic.entity";
import { ElementNature } from "../entities/elementNature.entity";
import { ElementUsageUnit } from "../entities/elementUsageUnit.entity";
import * as faker from "faker";

const ModuleSeed: Module[] = [
  {
    id: 1,
    name: "Mur - grand",
    lenght: 0,
    cctp: {} as Cctp,
    width: 0,
    principleCut: "Coupe 1",
    moduleFillingBetweenRising: {} as ModuleFillingBetweenRising,
    moduleRising: {} as ModuleRising,
    moduleSection: {} as ModuleSection,
    elementCharacteristic: {} as ElementCharacteristic,
    elementNature: {} as ElementNature,
    elementUsageUnit: {} as ElementUsageUnit,
    collections: [],
    components: []
  },
  {
    id: 2,
    name: "Mur - moyen",
    lenght: 0,
    cctp: {} as Cctp,
    width: 0,
    principleCut: "Coupe 2",
    moduleFillingBetweenRising: {} as ModuleFillingBetweenRising,
    moduleRising: {} as ModuleRising,
    moduleSection: {} as ModuleSection,
    elementCharacteristic: {} as ElementCharacteristic,
    elementNature: {} as ElementNature,
    elementUsageUnit: {} as ElementUsageUnit,
    collections: [],
    components: []
  },
  {
    id: 3,
    name: "Mur - petit",
    lenght: 0,
    cctp: {} as Cctp,
    width: 0,
    principleCut: "Coupe 3",
    moduleFillingBetweenRising: {} as ModuleFillingBetweenRising,
    moduleRising: {} as ModuleRising,
    moduleSection: {} as ModuleSection,
    elementCharacteristic: {} as ElementCharacteristic,
    elementNature: {} as ElementNature,
    elementUsageUnit: {} as ElementUsageUnit,
    collections: [],
    components: []
  },
  {
    id: 4,
    name: "Porte - simple",
    lenght: 0,
    cctp: {} as Cctp,
    width: 0,
    principleCut: "Coupe 4",
    moduleFillingBetweenRising: {} as ModuleFillingBetweenRising,
    moduleRising: {} as ModuleRising,
    moduleSection: {} as ModuleSection,
    elementCharacteristic: {} as ElementCharacteristic,
    elementNature: {} as ElementNature,
    elementUsageUnit: {} as ElementUsageUnit,
    collections: [],
    components: []
  },
  {
    id: 5,
    name: "Porte - double",
    lenght: 0,
    cctp: {} as Cctp,
    width: 0,
    principleCut: "Coupe 5",
    moduleFillingBetweenRising: {} as ModuleFillingBetweenRising,
    moduleRising: {} as ModuleRising,
    moduleSection: {} as ModuleSection,
    elementCharacteristic: {} as ElementCharacteristic,
    elementNature: {} as ElementNature,
    elementUsageUnit: {} as ElementUsageUnit,
    collections: [],
    components: []
  },
  {
    id: 6,
    name: "Fenêtre - simple",
    lenght: 0,
    cctp: {} as Cctp,
    width: 0,
    principleCut: "Coupe 6",
    moduleFillingBetweenRising: {} as ModuleFillingBetweenRising,
    moduleRising: {} as ModuleRising,
    moduleSection: {} as ModuleSection,
    elementCharacteristic: {} as ElementCharacteristic,
    elementNature: {} as ElementNature,
    elementUsageUnit: {} as ElementUsageUnit,
    collections: [],
    components: []
  },
  {
    id: 7,
    name: "Fenêtre - Double",
    lenght: 0,
    cctp: {} as Cctp,
    width: 0,
    principleCut: "Coupe 7",
    moduleFillingBetweenRising: {} as ModuleFillingBetweenRising,
    moduleRising: {} as ModuleRising,
    moduleSection: {} as ModuleSection,
    elementCharacteristic: {} as ElementCharacteristic,
    elementNature: {} as ElementNature,
    elementUsageUnit: {} as ElementUsageUnit,
    collections: [],
    components: []
  }
];

export const getModuleSeeder = () => {
  let modules: Module[] = ModuleSeed;

  modules.forEach(async m => {
    m.lenght = faker.random.number({ min: 1, max: 10000 });
    m.width = faker.random.number({ min: 1, max: 10000 });
    m.moduleFillingBetweenRising = {
      id: faker.random.number({ min: 1, max: 3 })
    } as any;
    m.moduleRising = { id: faker.random.number({ min: 1, max: 3 }) } as any;
    m.moduleSection = { id: faker.random.number({ min: 1, max: 5 }) } as any;
    m.elementCharacteristic = {
      id: faker.random.number({ min: 1, max: 6 })
    } as any;
    m.elementNature = { id: faker.random.number({ min: 1, max: 12 }) } as any;
    m.elementUsageUnit = { id: faker.random.number({ min: 1, max: 6 }) } as any;
    m.cctp = { id: faker.random.number({ min: 1, max: 7 }) } as any;
    m.components = [{ id: faker.random.number({ min: 1, max: 10 }) } as any];
  });

  return modules;
};
