import { Customer } from "../entities/customer.entity";
import * as faker from "faker";

export const getCustomerSeeder = (number: number) => {
  let customers: Customer[] = [];

  for (let i = 1; i < number; i++) {
    let customer = new Customer();
    customer.id = i;
    customer.firstName = faker.name.firstName();
    customer.lastName = faker.name.lastName();
    customer.tel = faker.phone.phoneNumber("06########");
    customer.zipcode = faker.address.zipCode();
    customer.city = faker.address.city();
    customer.address = `${faker.random.number(
      100
    )} ${faker.address.streetName()}`;
    customer.siret = faker.random.number(99999999999999).toString();
    customer.company = faker.company.companyName();

    customers.push(customer);
  }

  return customers;
};
