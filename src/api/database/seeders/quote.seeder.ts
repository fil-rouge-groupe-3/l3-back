import { Quote } from "../entities/quote.entity";
import { CrossSection } from "../entities/crossSection.entity";
import { PaymentMethod } from "../entities/paymentMethod.entity";
import { QuoteState } from "../entities/quoteState.entity";
import * as faker from "faker";
import moment = require("moment");

const QuoteSeed: Quote[] = [
  {
    id: 1,
    amoutHt: 55,
    crossSection: {} as CrossSection,
    createdAt: moment().format("L"),
    name: "Elaboration du jardin A - 4521",
    paymentMethod: {} as PaymentMethod,
    orders: [],
    projects: [],
    quoteState: {} as QuoteState
  },
  {
    id: 2,
    amoutHt: 55,
    crossSection: {} as CrossSection,
    createdAt: moment().format("L"),
    name: "Elaboration du jardin B - 4521",
    paymentMethod: {} as PaymentMethod,
    orders: [],
    projects: [],
    quoteState: {} as QuoteState
  },
  {
    id: 3,
    amoutHt: 55,
    crossSection: {} as CrossSection,
    createdAt: moment().format("L"),
    name: "Elaboration du jardin C - 4521",
    paymentMethod: {} as PaymentMethod,
    orders: [],
    projects: [],
    quoteState: {} as QuoteState
  },
  {
    id: 4,
    amoutHt: 55,
    crossSection: {} as CrossSection,
    createdAt: moment().format("L"),
    name: "Construction d'un Grand garage économe",
    paymentMethod: {} as PaymentMethod,
    orders: [],
    projects: [],
    quoteState: {} as QuoteState
  },
  {
    id: 5,
    amoutHt: 55,
    crossSection: {} as CrossSection,
    createdAt: moment().format("L"),
    name: "Elaboration d'un Jardin filtrant - 4521",
    paymentMethod: {} as PaymentMethod,
    orders: [],
    projects: [],
    quoteState: {} as QuoteState
  },
  {
    id: 6,
    amoutHt: 55,
    crossSection: {} as CrossSection,
    createdAt: moment().format("L"),
    name: "Création fondation de maison économe type 42-A",
    paymentMethod: {} as PaymentMethod,
    orders: [],
    projects: [],
    quoteState: {} as QuoteState
  },
  {
    id: 7,
    amoutHt: 55,
    crossSection: {} as CrossSection,
    createdAt: moment().format("L"),
    name: "Maison basse consommation type 42-A",
    paymentMethod: {} as PaymentMethod,
    orders: [],
    projects: [],
    quoteState: {} as QuoteState
  },
  {
    id: 8,
    amoutHt: 55,
    crossSection: {} as CrossSection,
    createdAt: moment().format("L"),
    name: "Extension de quai",
    paymentMethod: {} as PaymentMethod,
    orders: [],
    projects: [],
    quoteState: {} as QuoteState
  },
  {
    id: 9,
    amoutHt: 55,
    crossSection: {} as CrossSection,
    createdAt: moment().format("L"),
    name: "Abris de jardin économe",
    paymentMethod: {} as PaymentMethod,
    orders: [],
    projects: [],
    quoteState: {} as QuoteState
  },
  {
    id: 10,
    amoutHt: 55,
    crossSection: {} as CrossSection,
    createdAt: moment().format("L"),
    name: "RENOVATION - DOMAINE DE CHANTILLY",
    paymentMethod: {} as PaymentMethod,
    orders: [],
    projects: [],
    quoteState: {} as QuoteState
  }
];

export const getQuoteSeeder = () => {
  let quotes: Quote[] = QuoteSeed;

  quotes.forEach(async quote => {
    let crossSection = new CrossSection();
    let paymentMethod = new PaymentMethod();
    let quoteState = new QuoteState();

    crossSection.id = quote.id;
    paymentMethod.id = faker.random.number({ min: 1, max: 8 });
    quoteState.id = faker.random.number({ min: 1, max: 6 });
    quote.crossSection = crossSection;
    quote.paymentMethod = paymentMethod;
    quote.quoteState = quoteState;
    quote.amoutHt = faker.random.number(1000);
  });

  return quotes;
};
