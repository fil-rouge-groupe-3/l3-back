import { Floor } from "../entities/floor.entity";

export const FloorSeed: Floor[] = [
  {
    id: 1,
    name: "Sol stratifié aspect bois noir",
    overview:
      "https://www.saint-maclou.com/media/catalog/product/cache/1/base%20image/1540x940/9df78eab33525d08d6e5fb8d27136e95/9/8/98310_3581_p1.jpg",
    priceHt: "23,99"
  },
  {
    id: 2,
    name: "Sol stratifié aspect bois beige",
    overview:
      "https://www.saint-maclou.com/media/catalog/product/cache/1/base%20image/1540x940/9df78eab33525d08d6e5fb8d27136e95/9/8/98310_3574_p1.jpg",
    priceHt: "23,99"
  },
  {
    id: 3,
    name: "Sol vynil béton gris",
    overview:
      "https://www.saint-maclou.com/media/catalog/product/cache/1/base%20image/1540x940/9df78eab33525d08d6e5fb8d27136e95/8/9/8901_1990_p1.jpg",
    priceHt: "16,99"
  },
  {
    id: 4,
    name: "Sol vynil béton gris foncé",
    overview:
      "https://www.saint-maclou.com/media/catalog/product/cache/1/base%20image/1540x940/9df78eab33525d08d6e5fb8d27136e95/8/9/8901_969_p1.jpg",
    priceHt: "16,99"
  },
  {
    id: 5,
    name: "Sol vynil aspect carreaux de ciment",
    overview:
      "https://www.saint-maclou.com/media/catalog/product/cache/1/base%20image/1540x940/9df78eab33525d08d6e5fb8d27136e95/8/9/8914_990_p1.jpg",
    priceHt: "12,99"
  },
  {
    id: 6,
    name: "Parquet Merbeau rouge",
    overview:
      "https://www.saint-maclou.com/media/catalog/product/cache/1/base%20image/1540x940/9df78eab33525d08d6e5fb8d27136e95/9/1/91464_1_p1.jpg",
    priceHt: "89,99"
  },
  {
    id: 7,
    name: "Parquet Majeste chêne naturel",
    overview:
      "https://www.saint-maclou.com/media/catalog/product/cache/1/base%20image/1540x940/9df78eab33525d08d6e5fb8d27136e95/9/7/97005_1_p1.jpg",
    priceHt: "76,49"
  }
];
