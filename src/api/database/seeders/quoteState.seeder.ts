import { QuoteState } from "../entities/quoteState.entity";

export const QuoteStateSeed: QuoteState[] = [
  {
    id: 1,
    name: "Brouillon",
    description: "Le devis est en cours de création.",
    color: "#919191",
    quotes: []
  },
  {
    id: 2,
    name: "Accepté",
    description: "Le devis a était accepté par le client.",
    color: "#0bd600",
    quotes: []
  },
  {
    id: 3,
    name: "En attente",
    description: "Le devis est en attente de réponse de la part du client.",
    color: "#d8e300",
    quotes: []
  },
  {
    id: 4,
    name: "Refusé",
    description: "Le devis a était refusé par le client.",
    color: "#c20407",
    quotes: []
  },
  {
    id: 5,
    name: "En commande",
    description:
      "Les différents composants ont étaient commandé au près des fournisseurs rattachés.",
    color: "#8cab03",
    quotes: []
  },
  {
    id: 6,
    name: "Transfert en facturation",
    description:
      "Le devis est en cours de transfert vers le service de facturation.",
    color: "#069ed1",
    quotes: []
  }
];
