/* eslint-disable @typescript-eslint/no-explicit-any */
import { Project } from "../entities/project.entity";
import * as faker from "faker";
import moment = require("moment");

const ProjectSeed: Project[] = [
  {
    id: 1,
    name: "Maison économe",
    desc: "L'objectif de ce projet est de réaliser une maison économe.",
    ref: 0,
    createdAt: moment().format("L"),
    users: [],
    customers: [],
    quotes: [],
    updatedAt: "",
    startedAt: "",
    endedAt: ""
  },
  {
    id: 2,
    name: "Rénovation du domaine de Chantilly",
    desc: "Rénover dans son intégralité le domaine de Chantilly.",
    ref: 0,
    createdAt: moment().format("L"),
    users: [],
    customers: [],
    quotes: [],
    updatedAt: "",
    startedAt: "",
    endedAt: ""
  }
];

export const getProjectSeeder = () => {
  let projects: Project[] = ProjectSeed;

  projects.forEach(async project => {
    project.ref = faker.random.number({ min: 1, max: 10000 });
    project.quotes = [{ id: faker.random.number({ min: 1, max: 10 }) }] as any;
    project.customers = [
      { id: faker.random.number({ min: 1, max: 49 }) }
    ] as any;
    project.users = [{ id: faker.random.number({ min: 1, max: 49 }) }] as any;
  });

  return projects;
};
