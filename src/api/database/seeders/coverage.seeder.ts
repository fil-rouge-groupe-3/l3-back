import { Coverage } from "../entities/coverage.entity";

export const CoverageSeed: Coverage[] = [
  {
    id: 1,
    name: "Tuile Monier rouges",
    overview: "https://m1.lmcdn.fr/media/1/5b5612cc3711262a32414826/.jpg",
    priceHt: "1,78"
  },
  {
    id: 2,
    name: "Tuile Imerys noir brillant",
    overview:
      "https://m2.lmcdn.fr/media/1/5adee1133711264c8b9e7db8/.jpg?width=650&format=jpg",
    priceHt: "2,20"
  },
  {
    id: 3,
    name: "Tuile Imerys ardoise",
    overview:
      "https://m2.lmcdn.fr/media/1/5adee0681a8f312e825d0be2/.jpg?width=650&format=jpg",
    priceHt: "1,44"
  }
];
