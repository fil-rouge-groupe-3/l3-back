import { User } from "../entities/user.entity";
import * as faker from "faker";
import bcrypt = require("bcryptjs");
import { UserRole } from "../entities/userRole.entity";

export const getUserSeeder = (number: number) => {
  let users: User[] = [];
  let salt = bcrypt.genSaltSync(12);

  for (let i = 1; i < number; i++) {
    let user = new User();
    let role = new UserRole();
    user.id = i;
    user.firstName = faker.name.firstName();
    user.lastName = faker.name.lastName();
    user.email = faker.internet.email(
      user.firstName,
      user.lastName,
      "madera.com"
    );
    user.password = bcrypt.hashSync(faker.internet.password(10), salt);
    user.tel = faker.phone.phoneNumber("06########");
    user.zipcode = faker.address.zipCode();
    user.city = faker.address.city();
    user.address = `${faker.random.number(100)} ${faker.address.streetName()}`;
    user.avatar_img = faker.image.avatar();
    role.id = 4;
    user.userRole = role;
    users.push(user);
  }
  let roleAdmin = new UserRole();
  let roleProvider = new UserRole();
  let roleBE = new UserRole();
  let roleCom = new UserRole();

  // Push Administrateur
  let userAdmin = new User();
  roleAdmin.id = 1;
  userAdmin.id = number + 1;
  userAdmin.firstName = faker.name.firstName();
  userAdmin.lastName = faker.name.lastName();
  userAdmin.email = "admin@madera.com";
  userAdmin.avatar_img = faker.image.avatar();
  userAdmin.password = bcrypt.hashSync("madera2020admin", salt);
  userAdmin.userRole = roleAdmin;
  users.push(userAdmin);

  // Push Fournisseur
  let userProvider = new User();
  roleProvider.id = 3;
  userProvider.id = number + 2;
  userProvider.firstName = faker.name.firstName();
  userProvider.lastName = faker.name.lastName();
  userProvider.email = "fourn@madera.com";
  userProvider.avatar_img = faker.image.avatar();
  userProvider.password = bcrypt.hashSync("madera2020fourn", salt);
  userProvider.userRole = roleProvider;
  users.push(userProvider);

  // Push Bureau Etude
  let userBE = new User();
  roleBE.id = 2;
  userBE.id = number + 3;
  userBE.firstName = faker.name.firstName();
  userBE.lastName = faker.name.lastName();
  userBE.email = "be@madera.com";
  userBE.avatar_img = faker.image.avatar();
  userBE.password = bcrypt.hashSync("madera2020be", salt);
  userBE.userRole = roleBE;
  users.push(userBE);

  // Push Commercial
  let userCom = new User();
  roleCom.id = 4;
  userCom.id = number + 4;
  userCom.firstName = faker.name.firstName();
  userCom.lastName = faker.name.lastName();
  userCom.email = "com@madera.com";
  userCom.avatar_img = faker.image.avatar();
  userCom.password = bcrypt.hashSync("madera2020com", salt);
  userCom.userRole = roleCom;
  users.push(userCom);

  return users;
};
