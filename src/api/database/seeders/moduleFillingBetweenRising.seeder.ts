import { ModuleFillingBetweenRising } from "../entities/modules/moduleFillingBetweenRising.entity";

export const ModuleFillingBetweenRisingSeed: ModuleFillingBetweenRising[] = [
  {
    id: 1,
    name: "Remplissages"
  },
  {
    id: 2,
    name: "Visserie"
  },
  {
    id: 3,
    name: "Panneaux"
  }
];
