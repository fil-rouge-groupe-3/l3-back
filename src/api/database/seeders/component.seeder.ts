import { Component } from "../entities/components/component.entity";
import { ComponentGroup } from "../entities/components/componentGroup.entity";
import { ElementCharacteristic } from "../entities/elementCharacteristic.entity";
import { ElementNature } from "../entities/elementNature.entity";
import { ElementUsageUnit } from "../entities/elementUsageUnit.entity";
import { Provider } from "../entities/provider.entity";
import * as faker from "faker";

const ComponentSeed: Component[] = [
  {
    id: 1,
    componentGroup: {} as ComponentGroup,
    elementCharacteristic: {} as ElementCharacteristic,
    elementNature: {} as ElementNature,
    elementUsageUnit: {} as ElementUsageUnit,
    priceHt: 0,
    provider: {} as Provider,
    modules: [],
    orders: []
  },
  {
    id: 2,
    componentGroup: {} as ComponentGroup,
    elementCharacteristic: {} as ElementCharacteristic,
    elementNature: {} as ElementNature,
    elementUsageUnit: {} as ElementUsageUnit,
    priceHt: 0,
    provider: {} as Provider,
    modules: [],
    orders: []
  },
  {
    id: 3,
    componentGroup: {} as ComponentGroup,
    elementCharacteristic: {} as ElementCharacteristic,
    elementNature: {} as ElementNature,
    elementUsageUnit: {} as ElementUsageUnit,
    priceHt: 0,
    provider: {} as Provider,
    modules: [],
    orders: []
  },
  {
    id: 4,
    componentGroup: {} as ComponentGroup,
    elementCharacteristic: {} as ElementCharacteristic,
    elementNature: {} as ElementNature,
    elementUsageUnit: {} as ElementUsageUnit,
    priceHt: 0,
    provider: {} as Provider,
    modules: [],
    orders: []
  },
  {
    id: 5,
    componentGroup: {} as ComponentGroup,
    elementCharacteristic: {} as ElementCharacteristic,
    elementNature: {} as ElementNature,
    elementUsageUnit: {} as ElementUsageUnit,
    priceHt: 0,
    provider: {} as Provider,
    modules: [],
    orders: []
  },
  {
    id: 6,
    componentGroup: {} as ComponentGroup,
    elementCharacteristic: {} as ElementCharacteristic,
    elementNature: {} as ElementNature,
    elementUsageUnit: {} as ElementUsageUnit,
    priceHt: 0,
    provider: {} as Provider,
    modules: [],
    orders: []
  },
  {
    id: 7,
    componentGroup: {} as ComponentGroup,
    elementCharacteristic: {} as ElementCharacteristic,
    elementNature: {} as ElementNature,
    elementUsageUnit: {} as ElementUsageUnit,
    priceHt: 0,
    provider: {} as Provider,
    modules: [],
    orders: []
  },
  {
    id: 8,
    componentGroup: {} as ComponentGroup,
    elementCharacteristic: {} as ElementCharacteristic,
    elementNature: {} as ElementNature,
    elementUsageUnit: {} as ElementUsageUnit,
    priceHt: 0,
    provider: {} as Provider,
    modules: [],
    orders: []
  },
  {
    id: 9,
    componentGroup: {} as ComponentGroup,
    elementCharacteristic: {} as ElementCharacteristic,
    elementNature: {} as ElementNature,
    elementUsageUnit: {} as ElementUsageUnit,
    priceHt: 0,
    provider: {} as Provider,
    modules: [],
    orders: []
  },
  {
    id: 10,
    componentGroup: {} as ComponentGroup,
    elementCharacteristic: {} as ElementCharacteristic,
    elementNature: {} as ElementNature,
    elementUsageUnit: {} as ElementUsageUnit,
    priceHt: 0,
    provider: {} as Provider,
    modules: [],
    orders: []
  }
];

export const getComponentSeeder = () => {
  let components: Component[] = ComponentSeed;

  components.forEach(async component => {
    component.priceHt = faker.random.number({ min: 1, max: 100 });
    component.componentGroup.id = faker.random.number({ min: 1, max: 6 });
    component.elementCharacteristic.id = faker.random.number({
      min: 1,
      max: 5
    });
    component.elementNature.id = faker.random.number({ min: 1, max: 11 });
    component.elementUsageUnit.id = faker.random.number({ min: 1, max: 5 });
    component.provider.id = faker.random.number({ min: 1, max: 24 });
  });

  return components;
};
