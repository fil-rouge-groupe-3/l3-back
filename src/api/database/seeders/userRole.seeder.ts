import { UserRole } from "../entities/userRole.entity";

export const UserRoleSeed: UserRole[] = [
  {
    id: 1,
    name: "ADMIN",
    description: "Administrateur",
    users: []
  },
  {
    id: 2,
    name: "BE",
    description: "Bureau d'étude",
    users: []
  },
  {
    id: 3,
    name: "FOURN",
    description: "Fournisseur",
    users: []
  },
  {
    id: 4,
    name: "COMM",
    description: "Commercial",
    users: []
  }
];
