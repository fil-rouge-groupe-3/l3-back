import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class Floor {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  overview: string;

  @Column()
  priceHt: string;
}
