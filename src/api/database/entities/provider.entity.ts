import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class Provider {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  tel: string;

  @Column()
  website: string;

  @Column()
  email: string;

  @Column()
  address: string;

  @Column()
  zipcode: string;

  @Column()
  city: string;

  @Column()
  country: string;

  @Column()
  siret: string;
}
