import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  JoinColumn,
  ManyToOne,
  ManyToMany
} from "typeorm";
import { UserRole } from "./userRole.entity";
import { IsEmail, Length } from "class-validator";
import { Project } from "./project.entity";

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    nullable: true
  })
  firstName: string;

  @Column({
    nullable: true
  })
  lastName: string;

  @Column({
    unique: true
  })
  @IsEmail()
  email: string;

  @Column({
    unique: true,
    select: false
  })
  @Length(8, 20)
  password: string;

  @Column({
    nullable: true
  })
  zipcode: string;

  @Column({
    nullable: true
  })
  address: string;

  @Column({
    nullable: true
  })
  city: string;

  @Column({
    nullable: true
  })
  tel: string;

  @Column({
    nullable: true
  })
  avatar_img: string;

  @ManyToOne(
    () => UserRole,
    userRole => userRole.users
  )
  @JoinColumn()
  userRole: UserRole;

  @ManyToMany(
    () => Project,
    project => project.users
  )
  projects: Project[];
}
