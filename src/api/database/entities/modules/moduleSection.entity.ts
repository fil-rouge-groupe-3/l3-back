import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class ModuleSection {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;
}
