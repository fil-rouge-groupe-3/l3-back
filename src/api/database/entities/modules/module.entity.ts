import { Collection } from "../collection.entity";
import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
  ManyToMany
} from "typeorm";
import { Cctp } from "../cctp.entity";
import { ElementCharacteristic } from "../elementCharacteristic.entity";
import { ElementNature } from "../elementNature.entity";
import { ElementUsageUnit } from "../elementUsageUnit.entity";
import { ModuleSection } from "./moduleSection.entity";
import { ModuleRising } from "./moduleRising.entity";
import { ModuleFillingBetweenRising } from "./moduleFillingBetweenRising.entity";
import { Component } from "../components/component.entity";

@Entity()
export class Module {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  lenght: number;

  @Column()
  width: number;

  @Column()
  principleCut: string;

  @ManyToOne(() => Cctp)
  @JoinColumn()
  cctp: Cctp;

  @ManyToOne(() => ElementCharacteristic)
  @JoinColumn()
  elementCharacteristic: ElementCharacteristic;

  @ManyToOne(() => ElementNature)
  @JoinColumn()
  elementNature: ElementNature;

  @ManyToOne(() => ElementUsageUnit)
  @JoinColumn()
  elementUsageUnit: ElementUsageUnit;

  @ManyToOne(() => ModuleSection)
  @JoinColumn()
  moduleSection: ModuleSection;

  @ManyToOne(() => ModuleRising)
  @JoinColumn()
  moduleRising: ModuleRising;

  @ManyToOne(() => ModuleFillingBetweenRising)
  @JoinColumn()
  moduleFillingBetweenRising: ModuleFillingBetweenRising;

  @ManyToMany(
    () => Component,
    component => component.modules
  )
  components: Component[];

  @ManyToMany(
    () => Collection,
    collection => collection.modules
  )
  collections: Collection[];
}
