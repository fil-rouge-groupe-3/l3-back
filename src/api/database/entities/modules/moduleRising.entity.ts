import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class ModuleRising {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;
}
