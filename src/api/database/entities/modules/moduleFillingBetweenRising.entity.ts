import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class ModuleFillingBetweenRising {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;
}
