import { Project } from "./project.entity";
import { Entity, PrimaryGeneratedColumn, Column, ManyToMany } from "typeorm";

@Entity()
export class Customer {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column()
  company: string;

  @Column()
  siret: string;

  @Column()
  address: string;

  @Column()
  city: string;

  @Column()
  zipcode: string;

  @Column()
  tel: string;

  @ManyToMany(
    () => Project,
    project => project.customers
  )
  projects: Project[];
}
