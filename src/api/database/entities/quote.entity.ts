import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  ManyToMany,
  JoinTable,
  OneToMany
} from "typeorm";
import { PaymentMethod } from "./paymentMethod.entity";
import { QuoteState } from "./quoteState.entity";
import { CrossSection } from "./crossSection.entity";
import { Project } from "./project.entity";
import { Order } from "./order.entity";

@Entity()
export class Quote {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({ nullable: true })
  amoutHt: number;

  @ManyToOne(() => CrossSection)
  @JoinColumn()
  crossSection: CrossSection;

  @ManyToOne(() => PaymentMethod)
  @JoinColumn()
  paymentMethod: PaymentMethod;

  @ManyToOne(() => QuoteState)
  @JoinColumn()
  quoteState: QuoteState;

  @ManyToMany(
    () => Project,
    project => project.quotes
  )
  @JoinTable()
  projects: Project[];

  @OneToMany(
    () => Order,
    order => order.quote
  )
  orders: Order[];

  @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP" })
  createdAt: string;

  @Column({
    nullable: true,
    type: "timestamp"
  })
  updatedAt?: string;
}
