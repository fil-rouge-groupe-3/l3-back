import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class ElementNature {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;
}
