import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class Cctp {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;
}
