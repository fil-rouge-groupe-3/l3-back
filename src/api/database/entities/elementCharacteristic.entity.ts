import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class ElementCharacteristic {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;
}
