import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class DoorFrame {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  quality: string;
}
