import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn
} from "typeorm";
import { User } from "./user.entity";
import { Quote } from "./quote.entity";
import { Customer } from "./customer.entity";

@Entity()
export class Project {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  ref: number;

  @Column()
  name: string;

  @Column()
  desc: string;

  @ManyToMany(
    () => User,
    user => user.projects
  )
  @JoinTable()
  users: User[];

  @ManyToMany(
    () => Customer,
    customer => customer.projects
  )
  @JoinTable()
  customers: Customer[];

  @ManyToMany(
    () => Quote,
    quote => quote.projects
  )
  quotes: Quote[];

  @Column({ type: "timestamp" })
  startedAt: string;

  @Column({ type: "timestamp" })
  endedAt: string;

  @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP" })
  createdAt: string;

  @Column({
    nullable: true,
    type: "timestamp"
  })
  updatedAt: string;
}
