import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class ComponentGroup {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  description: string;
}
