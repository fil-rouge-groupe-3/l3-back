import { ElementUsageUnit } from "../elementUsageUnit.entity";
import { ComponentGroup } from "./componentGroup.entity";
import { Provider } from "../provider.entity";
import {
  Entity,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
  Column,
  OneToMany,
  ManyToMany,
  JoinTable
} from "typeorm";
import { ElementCharacteristic } from "../elementCharacteristic.entity";
import { ElementNature } from "../elementNature.entity";
import { Order } from "../order.entity";
import { Module } from "../modules/module.entity";

@Entity()
export class Component {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => ElementUsageUnit)
  @JoinColumn()
  elementUsageUnit: ElementUsageUnit;

  @ManyToOne(() => ComponentGroup)
  @JoinColumn()
  componentGroup: ComponentGroup;

  @ManyToOne(() => ElementCharacteristic)
  @JoinColumn()
  elementCharacteristic: ElementCharacteristic;

  @ManyToOne(() => ElementNature)
  @JoinColumn()
  elementNature: ElementNature;

  @ManyToOne(() => Provider)
  @JoinColumn()
  provider: Provider;

  @OneToMany(
    () => Order,
    order => order.component
  )
  orders: Order[];

  @ManyToMany(() => Module, m => m.components)
  @JoinTable()
  modules: Module[];

  @Column()
  priceHt: number;
}
