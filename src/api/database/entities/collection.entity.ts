import { CrossSection } from "./crossSection.entity";
import { DoorFrame } from "./doorFrame.entity";
import { Floor } from "./floor.entity";
import { Coverage } from "./coverage.entity";
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  JoinColumn,
  ManyToMany,
  JoinTable
} from "typeorm";
import { Module } from "./modules/module.entity";
import { Cctp } from "./cctp.entity";

@Entity()
export class Collection {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @ManyToOne(() => Cctp)
  @JoinColumn()
  cctp: Cctp;

  @ManyToOne(() => CrossSection)
  @JoinColumn()
  crossSection: CrossSection;

  @ManyToOne(() => DoorFrame)
  @JoinColumn()
  doorFrame: DoorFrame;

  @ManyToOne(() => Floor)
  @JoinColumn()
  floor: Floor;

  @ManyToOne(() => Coverage)
  @JoinColumn()
  coverage: Coverage;

  @ManyToMany(
    () => Module,
    module => module.collections
  )
  @JoinTable()
  modules: Module[];
}
