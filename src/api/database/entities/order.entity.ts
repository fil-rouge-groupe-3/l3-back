import { Entity, PrimaryGeneratedColumn, ManyToOne } from "typeorm";
import { Component } from "./components/component.entity";
import { Quote } from "./quote.entity";

@Entity()
export class Order {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Component)
  component: Component;

  @ManyToOne(() => Quote)
  quote: Quote;
}
