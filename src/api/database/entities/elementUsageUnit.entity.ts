import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class ElementUsageUnit {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;
}
