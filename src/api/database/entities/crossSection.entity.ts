import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";
import { Quote } from "./quote.entity";

@Entity()
export class CrossSection {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  blueprintName: string;

  @Column()
  blueprintPath: string;

  @OneToMany(
    () => Quote,
    quote => quote.crossSection
  )
  quotes: Quote[];
}
