import { Column, Entity, PrimaryGeneratedColumn, OneToMany } from "typeorm";
import { Quote } from "./quote.entity";

@Entity()
export class PaymentMethod {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  stepName: string;

  @Column()
  description: string;

  @Column()
  percentageToUnlock: number;

  @OneToMany(
    () => Quote,
    quote => quote.paymentMethod
  )
  quotes: Quote[];
}
