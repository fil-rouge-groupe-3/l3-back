import { Column, Entity, PrimaryGeneratedColumn, OneToMany } from "typeorm";
import { Quote } from "./quote.entity";

@Entity()
export class QuoteState {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  description: string;

  @Column()
  color: string;

  @OneToMany(
    () => Quote,
    quote => quote.quoteState
  )
  quotes: Quote[];
}
