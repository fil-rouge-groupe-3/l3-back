import { MigrationInterface, QueryRunner } from "typeorm";

export class AddFieldOnCollectionTable1577446002629
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      "ALTER TABLE `collection` ADD `cctpId` int NULL",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `collection` ADD CONSTRAINT `FK_514a2a0268227dfcc68a2e1b05d` FOREIGN KEY (`cctpId`) REFERENCES `cctp`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION",
      undefined
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      "ALTER TABLE `collection` DROP FOREIGN KEY `FK_514a2a0268227dfcc68a2e1b05d`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `collection` DROP COLUMN `cctpId`",
      undefined
    );
  }
}
