import {MigrationInterface, QueryRunner, getRepository} from "typeorm";
import { ModuleSection } from "../entities/modules/moduleSection.entity";
import { ModuleSectionSeed } from "../seeders/moduleSection.seeder";

export class SeedModuleSection1582389233241 implements MigrationInterface {

    public async up(_: QueryRunner): Promise<any> {
        await getRepository(ModuleSection).save(ModuleSectionSeed);
    }

    public async down(_: QueryRunner): Promise<any> {
    }

}
