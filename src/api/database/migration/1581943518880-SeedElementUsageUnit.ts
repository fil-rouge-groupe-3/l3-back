import { MigrationInterface, QueryRunner, getRepository } from "typeorm";
import { ElementUsageUnit } from "../entities/elementUsageUnit.entity";
import { ElementUsageUnitSeed } from "../seeders/entityUsageUnit.seeder";

export class SeedElementUsageUnit1581943518880 implements MigrationInterface {
  public async up(_: QueryRunner): Promise<any> {
    await getRepository(ElementUsageUnit).save(ElementUsageUnitSeed);
  }

  public async down(_: QueryRunner): Promise<any> {}
}
