import { MigrationInterface, QueryRunner, getRepository } from "typeorm";
import { Floor } from "../entities/floor.entity";
import { FloorSeed } from "../seeders/floor.seeder";

export class SeedFloor1583329684248 implements MigrationInterface {
  public async up(_: QueryRunner): Promise<any> {
    await getRepository(Floor).save(FloorSeed);
  }

  public async down(_: QueryRunner): Promise<any> {}
}
