import { MigrationInterface, QueryRunner, getRepository } from "typeorm";
import { ElementNature } from "../entities/elementNature.entity";
import { ElementNatureSeed } from "../seeders/entityNature.seeder";

export class SeedElementNature1581942531803 implements MigrationInterface {
  public async up(_: QueryRunner): Promise<any> {
    await getRepository(ElementNature).save(ElementNatureSeed);
  }

  public async down(_: QueryRunner): Promise<any> {}
}
