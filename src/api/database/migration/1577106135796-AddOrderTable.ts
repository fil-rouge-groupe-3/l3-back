import { MigrationInterface, QueryRunner } from "typeorm";

export class AddOrderTable1577106135796 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      "CREATE TABLE `order` (`id` int NOT NULL AUTO_INCREMENT, `componentId` int NULL, `moduleId` int NULL, `quoteId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `component` ADD `priceHt` int NOT NULL",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `order` ADD CONSTRAINT `FK_9d87d617f139dcf1a2a69b20e18` FOREIGN KEY (`componentId`) REFERENCES `component`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `order` ADD CONSTRAINT `FK_899a2659287aeecd7378089ef14` FOREIGN KEY (`moduleId`) REFERENCES `module`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `order` ADD CONSTRAINT `FK_b8a526d989d2db4b9aaa3d6cf5d` FOREIGN KEY (`quoteId`) REFERENCES `quote`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION",
      undefined
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      "ALTER TABLE `order` DROP FOREIGN KEY `FK_b8a526d989d2db4b9aaa3d6cf5d`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `order` DROP FOREIGN KEY `FK_899a2659287aeecd7378089ef14`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `order` DROP FOREIGN KEY `FK_9d87d617f139dcf1a2a69b20e18`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `component` DROP COLUMN `priceHt`",
      undefined
    );
    await queryRunner.query("DROP TABLE `order`", undefined);
  }
}
