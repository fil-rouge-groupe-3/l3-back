import { MigrationInterface, QueryRunner, getRepository } from "typeorm";
import { Quote } from "../entities/quote.entity";
import { getQuoteSeeder } from "../seeders/quote.seeder";
import { PaymentMethod } from "../entities/paymentMethod.entity";
import { PaymentMethodSeed } from "../seeders/paymentMethod.seeder";

export class SeedQuote1580302364755 implements MigrationInterface {
  public async up(_: QueryRunner): Promise<any> {
    await getRepository(PaymentMethod).save(PaymentMethodSeed);
    await getRepository(Quote).save(getQuoteSeeder());
  }

  public async down(_: QueryRunner): Promise<any> {}
}
