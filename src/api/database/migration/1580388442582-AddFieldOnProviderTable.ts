import { MigrationInterface, QueryRunner } from "typeorm";

export class AddFieldOnProviderTable1580388442582
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      "ALTER TABLE `provider` ADD `tel` varchar(255) NOT NULL",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `provider` ADD `website` varchar(255) NOT NULL",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `provider` ADD `email` varchar(255) NOT NULL",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `provider` ADD `address` varchar(255) NOT NULL",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `provider` ADD `zipcode` varchar(255) NOT NULL",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `provider` ADD `city` varchar(255) NOT NULL",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `provider` ADD `country` varchar(255) NOT NULL",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `provider` ADD `siret` varchar(255) NOT NULL",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `customer` CHANGE `city` `city` varchar(255) NOT NULL",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `quote` CHANGE `createdAt` `createdAt` varchar(255) NOT NULL DEFAULT '01/30/2020'",
      undefined
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      "ALTER TABLE `quote` CHANGE `createdAt` `createdAt` varchar(255) NOT NULL DEFAULT ''01/28/2020''",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `customer` CHANGE `city` `city` varchar(255) NULL",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `provider` DROP COLUMN `siret`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `provider` DROP COLUMN `country`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `provider` DROP COLUMN `city`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `provider` DROP COLUMN `zipcode`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `provider` DROP COLUMN `address`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `provider` DROP COLUMN `email`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `provider` DROP COLUMN `website`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `provider` DROP COLUMN `tel`",
      undefined
    );
  }
}
