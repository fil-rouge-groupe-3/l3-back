import { MigrationInterface, QueryRunner } from "typeorm";

export class AddDescriptionFieldOnComponentGroup1581273557746
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      "ALTER TABLE `component_group` ADD `description` varchar(255) NOT NULL",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `project` CHANGE `startedAt` `startedAt` timestamp NOT NULL",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `project` CHANGE `endedAt` `endedAt` timestamp NOT NULL",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `project` CHANGE `createdAt` `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `quote` CHANGE `createdAt` `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP",
      undefined
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      "ALTER TABLE `quote` CHANGE `createdAt` `createdAt` timestamp NOT NULL DEFAULT 'current_timestamp()'",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `project` CHANGE `createdAt` `createdAt` timestamp NOT NULL DEFAULT 'current_timestamp()'",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `project` CHANGE `endedAt` `endedAt` timestamp NULL",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `project` CHANGE `startedAt` `startedAt` timestamp NULL",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `component_group` DROP COLUMN `description`",
      undefined
    );
  }
}
