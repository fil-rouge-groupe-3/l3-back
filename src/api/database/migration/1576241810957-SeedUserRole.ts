import { MigrationInterface, QueryRunner, getRepository } from "typeorm";
import { UserRole } from "../entities/userRole.entity";
import { UserRoleSeed } from "../seeders/userRole.seeder";

export class SeedUserRole1576241810957 implements MigrationInterface {
  public async up(_: QueryRunner): Promise<any> {
    await getRepository(UserRole).save(UserRoleSeed);
  }

  public async down(_: QueryRunner): Promise<any> {}
}
