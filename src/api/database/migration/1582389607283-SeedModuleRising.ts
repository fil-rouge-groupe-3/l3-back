import {MigrationInterface, QueryRunner, getRepository} from "typeorm";
import { ModuleRising } from "../entities/modules/moduleRising.entity";
import { ModuleRisingSeed } from "../seeders/moduleRising.seeder";

export class SeedModuleRising1582389607283 implements MigrationInterface {

    public async up(_: QueryRunner): Promise<any> {
        await getRepository(ModuleRising).save(ModuleRisingSeed)
    }

    public async down(_: QueryRunner): Promise<any> {
    }

}
