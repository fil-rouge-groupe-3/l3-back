import { MigrationInterface, QueryRunner, getRepository } from "typeorm";
import { Provider } from "../entities/provider.entity";
import { getProviderSeeder } from "../seeders/provider.seeder";

export class SeedProvider1580388578666 implements MigrationInterface {
  public async up(_: QueryRunner): Promise<any> {
    await getRepository(Provider).save(getProviderSeeder(25));
  }

  public async down(_: QueryRunner): Promise<any> {}
}
