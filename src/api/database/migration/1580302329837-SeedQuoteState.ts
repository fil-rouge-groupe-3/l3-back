import { MigrationInterface, QueryRunner, getRepository } from "typeorm";
import { QuoteState } from "../entities/quoteState.entity";
import { QuoteStateSeed } from "../seeders/quoteState.seeder";

export class SeedQuoteState1580302329837 implements MigrationInterface {
  public async up(_: QueryRunner): Promise<any> {
    await getRepository(QuoteState).save(QuoteStateSeed);
  }

  public async down(_: QueryRunner): Promise<any> {}
}
