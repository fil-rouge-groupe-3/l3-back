import { MigrationInterface, QueryRunner, getRepository } from "typeorm";
import { CrossSection } from "../entities/crossSection.entity";
import { CrossSectionSeed } from "../seeders/crossSection.seeder";

export class SeedCrossSection1580302350043 implements MigrationInterface {
  public async up(_: QueryRunner): Promise<any> {
    await getRepository(CrossSection).save(CrossSectionSeed);
  }

  public async down(_: QueryRunner): Promise<any> {}
}
