import {MigrationInterface, QueryRunner, getRepository} from "typeorm";
import { ModuleFillingBetweenRising } from "../entities/modules/moduleFillingBetweenRising.entity";
import { ModuleFillingBetweenRisingSeed } from "../seeders/moduleFillingBetweenRising.seeder";

export class SeedModuleFillingBetweenRising1582390704875 implements MigrationInterface {

    public async up(_: QueryRunner): Promise<any> {
        await getRepository(ModuleFillingBetweenRising).save(ModuleFillingBetweenRisingSeed)
    }

    public async down(_: QueryRunner): Promise<any> {
    }

}
