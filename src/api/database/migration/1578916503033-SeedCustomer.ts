import { MigrationInterface, QueryRunner, getRepository } from "typeorm";
import { Customer } from "../entities/customer.entity";
import { getCustomerSeeder } from "../seeders/customer.seeder";

export class SeedCustomer1578916503033 implements MigrationInterface {
  public async up(_: QueryRunner): Promise<any> {
    await getRepository(Customer).save(getCustomerSeeder(50));
  }

  public async down(_: QueryRunner): Promise<any> {}
}
