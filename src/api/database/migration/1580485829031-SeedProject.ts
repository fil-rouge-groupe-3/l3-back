import { MigrationInterface, QueryRunner, getRepository } from "typeorm";
import { Project } from "../entities/project.entity";
import { getProjectSeeder } from "../seeders/project.seeder";

export class SeedProject1580485829031 implements MigrationInterface {
  public async up(_: QueryRunner): Promise<any> {
    await getRepository(Project).save(getProjectSeeder());
  }

  public async down(_: QueryRunner): Promise<any> {}
}
