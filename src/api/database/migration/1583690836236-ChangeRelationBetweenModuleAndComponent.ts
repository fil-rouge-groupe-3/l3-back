import {MigrationInterface, QueryRunner} from "typeorm";

export class ChangeRelationBetweenModuleAndComponent1583690836236 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `module` DROP FOREIGN KEY `FK_551ad404c26e0bd8463697cea8a`", undefined);
        await queryRunner.query("ALTER TABLE `order` DROP FOREIGN KEY `FK_899a2659287aeecd7378089ef14`", undefined);
        await queryRunner.query("CREATE TABLE `component_modules_module` (`componentId` int NOT NULL, `moduleId` int NOT NULL, INDEX `IDX_931e069ddab3aae7cd9412b195` (`componentId`), INDEX `IDX_9362761a2e8f0321a1d90f53a2` (`moduleId`), PRIMARY KEY (`componentId`, `moduleId`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("ALTER TABLE `order` DROP COLUMN `moduleId`", undefined);
        await queryRunner.query("ALTER TABLE `module` ADD CONSTRAINT `FK_3b726912ab58ee15be8b0a22623` FOREIGN KEY (`moduleFillingBetweenRisingId`) REFERENCES `module_filling_between_rising`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION", undefined);
        await queryRunner.query("ALTER TABLE `component_modules_module` ADD CONSTRAINT `FK_931e069ddab3aae7cd9412b195a` FOREIGN KEY (`componentId`) REFERENCES `component`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION", undefined);
        await queryRunner.query("ALTER TABLE `component_modules_module` ADD CONSTRAINT `FK_9362761a2e8f0321a1d90f53a20` FOREIGN KEY (`moduleId`) REFERENCES `module`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `component_modules_module` DROP FOREIGN KEY `FK_9362761a2e8f0321a1d90f53a20`", undefined);
        await queryRunner.query("ALTER TABLE `component_modules_module` DROP FOREIGN KEY `FK_931e069ddab3aae7cd9412b195a`", undefined);
        await queryRunner.query("ALTER TABLE `module` DROP FOREIGN KEY `FK_3b726912ab58ee15be8b0a22623`", undefined);
        await queryRunner.query("ALTER TABLE `order` ADD `moduleId` int NULL", undefined);
        await queryRunner.query("DROP INDEX `IDX_9362761a2e8f0321a1d90f53a2` ON `component_modules_module`", undefined);
        await queryRunner.query("DROP INDEX `IDX_931e069ddab3aae7cd9412b195` ON `component_modules_module`", undefined);
        await queryRunner.query("DROP TABLE `component_modules_module`", undefined);
        await queryRunner.query("ALTER TABLE `order` ADD CONSTRAINT `FK_899a2659287aeecd7378089ef14` FOREIGN KEY (`moduleId`) REFERENCES `module`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION", undefined);
        await queryRunner.query("ALTER TABLE `module` ADD CONSTRAINT `FK_551ad404c26e0bd8463697cea8a` FOREIGN KEY (`moduleFillingBetweenRisingId`) REFERENCES `module_filling_between_rising`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION", undefined);
    }

}
