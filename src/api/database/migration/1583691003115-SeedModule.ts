import {MigrationInterface, QueryRunner, getRepository} from "typeorm";
import { Module } from "../entities/modules/module.entity";
import { getModuleSeeder } from "../seeders/module.seeder";

export class SeedModule1583691003115 implements MigrationInterface {

    public async up(_: QueryRunner): Promise<any> {
        await getRepository(Module).save(getModuleSeeder());
    }

    public async down(_: QueryRunner): Promise<any> {
    }

}
