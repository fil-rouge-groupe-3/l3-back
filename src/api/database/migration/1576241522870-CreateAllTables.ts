import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateAllTables1576241522870 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      "CREATE TABLE `calculation_rule` (`id` int NOT NULL AUTO_INCREMENT, `name` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB",
      undefined
    );
    await queryRunner.query(
      "CREATE TABLE `cctp` (`id` int NOT NULL AUTO_INCREMENT, `name` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB",
      undefined
    );
    await queryRunner.query(
      "CREATE TABLE `cross_section` (`id` int NOT NULL AUTO_INCREMENT, `blueprintName` varchar(255) NOT NULL, `blueprintPath` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB",
      undefined
    );
    await queryRunner.query(
      "CREATE TABLE `door_frame` (`id` int NOT NULL AUTO_INCREMENT, `name` varchar(255) NOT NULL, `quality` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB",
      undefined
    );
    await queryRunner.query(
      "CREATE TABLE `floor` (`id` int NOT NULL AUTO_INCREMENT, `name` varchar(255) NOT NULL, `overview` varchar(255) NOT NULL, `priceHt` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB",
      undefined
    );
    await queryRunner.query(
      "CREATE TABLE `coverage` (`id` int NOT NULL AUTO_INCREMENT, `name` varchar(255) NOT NULL, `overview` varchar(255) NOT NULL, `priceHt` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB",
      undefined
    );
    await queryRunner.query(
      "CREATE TABLE `component_usage_unit` (`id` int NOT NULL AUTO_INCREMENT, `name` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB",
      undefined
    );
    await queryRunner.query(
      "CREATE TABLE `component_group` (`id` int NOT NULL AUTO_INCREMENT, `name` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB",
      undefined
    );
    await queryRunner.query(
      "CREATE TABLE `provider` (`id` int NOT NULL AUTO_INCREMENT, `name` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB",
      undefined
    );
    await queryRunner.query(
      "CREATE TABLE `component_characteristic` (`id` int NOT NULL AUTO_INCREMENT, `name` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB",
      undefined
    );
    await queryRunner.query(
      "CREATE TABLE `component_nature` (`id` int NOT NULL AUTO_INCREMENT, `name` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB",
      undefined
    );
    await queryRunner.query(
      "CREATE TABLE `component` (`id` int NOT NULL AUTO_INCREMENT, `componentUsageUnitId` int NULL, `componentGroupId` int NULL, `componentCharacteristicId` int NULL, `componentNatureId` int NULL, `providerId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB",
      undefined
    );
    await queryRunner.query(
      "CREATE TABLE `module` (`id` int NOT NULL AUTO_INCREMENT, `name` varchar(255) NOT NULL, `lenght` int NOT NULL, `width` int NOT NULL, `principleCut` varchar(255) NOT NULL, `cctpId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB",
      undefined
    );
    await queryRunner.query(
      "CREATE TABLE `collection` (`id` int NOT NULL AUTO_INCREMENT, `name` varchar(255) NOT NULL, `crossSectionId` int NULL, `doorFrameId` int NULL, `calculationRuleId` int NULL, `floorId` int NULL, `coverageId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB",
      undefined
    );
    await queryRunner.query(
      "CREATE TABLE `user_role` (`id` int NOT NULL AUTO_INCREMENT, `name` varchar(255) NOT NULL, `description` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB",
      undefined
    );
    await queryRunner.query(
      "CREATE TABLE `payment_method` (`id` int NOT NULL AUTO_INCREMENT, `step` varchar(255) NOT NULL, `percentageToUnlock` int NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB",
      undefined
    );
    await queryRunner.query(
      "CREATE TABLE `quote_state` (`id` int NOT NULL AUTO_INCREMENT, `name` varchar(255) NOT NULL, `color` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB",
      undefined
    );
    await queryRunner.query(
      "CREATE TABLE `quote` (`id` int NOT NULL AUTO_INCREMENT, `name` varchar(255) NOT NULL, `amoutHt` int NULL, `amoutTtc` int NOT NULL, `crossSectionId` int NULL, `paymentMethodId` int NULL, `quoteStateId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB",
      undefined
    );
    await queryRunner.query(
      "CREATE TABLE `user` (`id` int NOT NULL AUTO_INCREMENT, `firstName` varchar(255) NULL, `lastName` varchar(255) NULL, `email` varchar(255) NOT NULL, `password` varchar(255) NOT NULL, `zipcode` varchar(255) NULL, `address` varchar(255) NULL, `city` varchar(255) NULL, `tel` varchar(255) NULL, `avatar_img` varchar(255) NULL, `userRoleId` int NULL, UNIQUE INDEX `IDX_e12875dfb3b1d92d7d7c5377e2` (`email`), UNIQUE INDEX `IDX_638bac731294171648258260ff` (`password`), PRIMARY KEY (`id`)) ENGINE=InnoDB",
      undefined
    );
    await queryRunner.query(
      "CREATE TABLE `project` (`id` int NOT NULL AUTO_INCREMENT, `ref` int NOT NULL, `name` varchar(255) NOT NULL, `desc` varchar(255) NOT NULL, `createdAt` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB",
      undefined
    );
    await queryRunner.query(
      "CREATE TABLE `customer` (`id` int NOT NULL AUTO_INCREMENT, `firstName` varchar(255) NOT NULL, `lastName` varchar(255) NOT NULL, `company` varchar(255) NOT NULL, `siret` varchar(255) NOT NULL, `address` varchar(255) NOT NULL, `city` varchar(255) NULL, `zipcode` varchar(255) NOT NULL, `tel` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB",
      undefined
    );
    await queryRunner.query(
      "CREATE TABLE `collection_modules_module` (`collectionId` int NOT NULL, `moduleId` int NOT NULL, INDEX `IDX_819901d7122dd0fa90f7035035` (`collectionId`), INDEX `IDX_f3925858c214951827d191b2d8` (`moduleId`), PRIMARY KEY (`collectionId`, `moduleId`)) ENGINE=InnoDB",
      undefined
    );
    await queryRunner.query(
      "CREATE TABLE `quote_projects_project` (`quoteId` int NOT NULL, `projectId` int NOT NULL, INDEX `IDX_5f9853c49132f09020f2349e72` (`quoteId`), INDEX `IDX_b54a418319f77829a94b18baa3` (`projectId`), PRIMARY KEY (`quoteId`, `projectId`)) ENGINE=InnoDB",
      undefined
    );
    await queryRunner.query(
      "CREATE TABLE `project_users_user` (`projectId` int NOT NULL, `userId` int NOT NULL, INDEX `IDX_9666c6dcd769c698bed4aa4bf5` (`projectId`), INDEX `IDX_f8300efd87679e1e21532be980` (`userId`), PRIMARY KEY (`projectId`, `userId`)) ENGINE=InnoDB",
      undefined
    );
    await queryRunner.query(
      "CREATE TABLE `project_customers_customer` (`projectId` int NOT NULL, `customerId` int NOT NULL, INDEX `IDX_913a9d9a26b8cde6a2b2b4d113` (`projectId`), INDEX `IDX_b15a2d24fe72e57a64c816e92f` (`customerId`), PRIMARY KEY (`projectId`, `customerId`)) ENGINE=InnoDB",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `component` ADD CONSTRAINT `FK_10b45b92f377152d17843d6f3fa` FOREIGN KEY (`componentUsageUnitId`) REFERENCES `component_usage_unit`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `component` ADD CONSTRAINT `FK_a00f98c49b4bc2b95751e47688a` FOREIGN KEY (`componentGroupId`) REFERENCES `component_group`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `component` ADD CONSTRAINT `FK_ee5d6b1a01f9fa735d899967080` FOREIGN KEY (`componentCharacteristicId`) REFERENCES `component_characteristic`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `component` ADD CONSTRAINT `FK_100db4386e85a85e84c6c125275` FOREIGN KEY (`componentNatureId`) REFERENCES `component_nature`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `component` ADD CONSTRAINT `FK_adc1c586b4906ca315c0a995a94` FOREIGN KEY (`providerId`) REFERENCES `provider`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `module` ADD CONSTRAINT `FK_cff75f150871a9fbaac9e806302` FOREIGN KEY (`cctpId`) REFERENCES `cctp`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `collection` ADD CONSTRAINT `FK_1abaf7d7648fa4f132029473281` FOREIGN KEY (`crossSectionId`) REFERENCES `cross_section`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `collection` ADD CONSTRAINT `FK_be3f11b0b254cac904eb1a774c0` FOREIGN KEY (`doorFrameId`) REFERENCES `door_frame`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `collection` ADD CONSTRAINT `FK_2aba6966a36688d00a10b49c904` FOREIGN KEY (`calculationRuleId`) REFERENCES `calculation_rule`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `collection` ADD CONSTRAINT `FK_26391e12395d2f6c032377986ae` FOREIGN KEY (`floorId`) REFERENCES `floor`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `collection` ADD CONSTRAINT `FK_2440ac1d0cbfd992395cce84580` FOREIGN KEY (`coverageId`) REFERENCES `coverage`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `quote` ADD CONSTRAINT `FK_063f2ad1341a44303abcc20341f` FOREIGN KEY (`crossSectionId`) REFERENCES `cross_section`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `quote` ADD CONSTRAINT `FK_1ccee9720d84d1848c05add4566` FOREIGN KEY (`paymentMethodId`) REFERENCES `payment_method`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `quote` ADD CONSTRAINT `FK_343b48a78b7b5088a1f517d3ec5` FOREIGN KEY (`quoteStateId`) REFERENCES `quote_state`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `user` ADD CONSTRAINT `FK_72292a143eb57e1189603308430` FOREIGN KEY (`userRoleId`) REFERENCES `user_role`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `collection_modules_module` ADD CONSTRAINT `FK_819901d7122dd0fa90f7035035e` FOREIGN KEY (`collectionId`) REFERENCES `collection`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `collection_modules_module` ADD CONSTRAINT `FK_f3925858c214951827d191b2d8b` FOREIGN KEY (`moduleId`) REFERENCES `module`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `quote_projects_project` ADD CONSTRAINT `FK_5f9853c49132f09020f2349e720` FOREIGN KEY (`quoteId`) REFERENCES `quote`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `quote_projects_project` ADD CONSTRAINT `FK_b54a418319f77829a94b18baa35` FOREIGN KEY (`projectId`) REFERENCES `project`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `project_users_user` ADD CONSTRAINT `FK_9666c6dcd769c698bed4aa4bf55` FOREIGN KEY (`projectId`) REFERENCES `project`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `project_users_user` ADD CONSTRAINT `FK_f8300efd87679e1e21532be9808` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `project_customers_customer` ADD CONSTRAINT `FK_913a9d9a26b8cde6a2b2b4d1131` FOREIGN KEY (`projectId`) REFERENCES `project`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `project_customers_customer` ADD CONSTRAINT `FK_b15a2d24fe72e57a64c816e92fc` FOREIGN KEY (`customerId`) REFERENCES `customer`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION",
      undefined
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      "ALTER TABLE `project_customers_customer` DROP FOREIGN KEY `FK_b15a2d24fe72e57a64c816e92fc`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `project_customers_customer` DROP FOREIGN KEY `FK_913a9d9a26b8cde6a2b2b4d1131`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `project_users_user` DROP FOREIGN KEY `FK_f8300efd87679e1e21532be9808`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `project_users_user` DROP FOREIGN KEY `FK_9666c6dcd769c698bed4aa4bf55`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `quote_projects_project` DROP FOREIGN KEY `FK_b54a418319f77829a94b18baa35`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `quote_projects_project` DROP FOREIGN KEY `FK_5f9853c49132f09020f2349e720`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `collection_modules_module` DROP FOREIGN KEY `FK_f3925858c214951827d191b2d8b`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `collection_modules_module` DROP FOREIGN KEY `FK_819901d7122dd0fa90f7035035e`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `component_modules_module` DROP FOREIGN KEY `FK_9362761a2e8f0321a1d90f53a20`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `component_modules_module` DROP FOREIGN KEY `FK_931e069ddab3aae7cd9412b195a`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `user` DROP FOREIGN KEY `FK_72292a143eb57e1189603308430`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `quote` DROP FOREIGN KEY `FK_343b48a78b7b5088a1f517d3ec5`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `quote` DROP FOREIGN KEY `FK_1ccee9720d84d1848c05add4566`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `quote` DROP FOREIGN KEY `FK_063f2ad1341a44303abcc20341f`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `collection` DROP FOREIGN KEY `FK_2440ac1d0cbfd992395cce84580`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `collection` DROP FOREIGN KEY `FK_26391e12395d2f6c032377986ae`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `collection` DROP FOREIGN KEY `FK_2aba6966a36688d00a10b49c904`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `collection` DROP FOREIGN KEY `FK_be3f11b0b254cac904eb1a774c0`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `collection` DROP FOREIGN KEY `FK_1abaf7d7648fa4f132029473281`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `module` DROP FOREIGN KEY `FK_cff75f150871a9fbaac9e806302`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `component` DROP FOREIGN KEY `FK_adc1c586b4906ca315c0a995a94`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `component` DROP FOREIGN KEY `FK_100db4386e85a85e84c6c125275`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `component` DROP FOREIGN KEY `FK_ee5d6b1a01f9fa735d899967080`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `component` DROP FOREIGN KEY `FK_a00f98c49b4bc2b95751e47688a`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `component` DROP FOREIGN KEY `FK_10b45b92f377152d17843d6f3fa`",
      undefined
    );
    await queryRunner.query(
      "DROP INDEX `IDX_b15a2d24fe72e57a64c816e92f` ON `project_customers_customer`",
      undefined
    );
    await queryRunner.query(
      "DROP INDEX `IDX_913a9d9a26b8cde6a2b2b4d113` ON `project_customers_customer`",
      undefined
    );
    await queryRunner.query(
      "DROP TABLE `project_customers_customer`",
      undefined
    );
    await queryRunner.query(
      "DROP INDEX `IDX_f8300efd87679e1e21532be980` ON `project_users_user`",
      undefined
    );
    await queryRunner.query(
      "DROP INDEX `IDX_9666c6dcd769c698bed4aa4bf5` ON `project_users_user`",
      undefined
    );
    await queryRunner.query("DROP TABLE `project_users_user`", undefined);
    await queryRunner.query(
      "DROP INDEX `IDX_b54a418319f77829a94b18baa3` ON `quote_projects_project`",
      undefined
    );
    await queryRunner.query(
      "DROP INDEX `IDX_5f9853c49132f09020f2349e72` ON `quote_projects_project`",
      undefined
    );
    await queryRunner.query("DROP TABLE `quote_projects_project`", undefined);
    await queryRunner.query(
      "DROP INDEX `IDX_a0c7b80c93e821058411c06b9c` ON `quote_users_user`",
      undefined
    );
    await queryRunner.query(
      "DROP INDEX `IDX_3f6572a79cb250dbe8cd430929` ON `quote_users_user`",
      undefined
    );
    await queryRunner.query(
      "DROP INDEX `IDX_f3925858c214951827d191b2d8` ON `collection_modules_module`",
      undefined
    );
    await queryRunner.query(
      "DROP INDEX `IDX_819901d7122dd0fa90f7035035` ON `collection_modules_module`",
      undefined
    );
    await queryRunner.query(
      "DROP TABLE `collection_modules_module`",
      undefined
    );
    await queryRunner.query(
      "DROP INDEX `IDX_9362761a2e8f0321a1d90f53a2` ON `component_modules_module`",
      undefined
    );
    await queryRunner.query(
      "DROP INDEX `IDX_931e069ddab3aae7cd9412b195` ON `component_modules_module`",
      undefined
    );
    await queryRunner.query("DROP TABLE `component_modules_module`", undefined);
    await queryRunner.query("DROP TABLE `customer`", undefined);
    await queryRunner.query("DROP TABLE `project`", undefined);
    await queryRunner.query(
      "DROP INDEX `IDX_638bac731294171648258260ff` ON `user`",
      undefined
    );
    await queryRunner.query(
      "DROP INDEX `IDX_e12875dfb3b1d92d7d7c5377e2` ON `user`",
      undefined
    );
    await queryRunner.query("DROP TABLE `user`", undefined);
    await queryRunner.query("DROP TABLE `quote`", undefined);
    await queryRunner.query("DROP TABLE `quote_state`", undefined);
    await queryRunner.query("DROP TABLE `payment_method`", undefined);
    await queryRunner.query("DROP TABLE `user_role`", undefined);
    await queryRunner.query("DROP TABLE `collection`", undefined);
    await queryRunner.query("DROP TABLE `module`", undefined);
    await queryRunner.query("DROP TABLE `component`", undefined);
    await queryRunner.query("DROP TABLE `component_nature`", undefined);
    await queryRunner.query("DROP TABLE `component_characteristic`", undefined);
    await queryRunner.query("DROP TABLE `provider`", undefined);
    await queryRunner.query("DROP TABLE `component_group`", undefined);
    await queryRunner.query("DROP TABLE `component_usage_unit`", undefined);
    await queryRunner.query("DROP TABLE `coverage`", undefined);
    await queryRunner.query("DROP TABLE `floor`", undefined);
    await queryRunner.query("DROP TABLE `door_frame`", undefined);
    await queryRunner.query("DROP TABLE `cross_section`", undefined);
    await queryRunner.query("DROP TABLE `cctp`", undefined);
    await queryRunner.query("DROP TABLE `calculation_rule`", undefined);
  }
}
