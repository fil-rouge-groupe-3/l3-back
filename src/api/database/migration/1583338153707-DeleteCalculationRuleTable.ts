import { MigrationInterface, QueryRunner } from "typeorm";

export class DeleteCalculationRuleTable1583338153707
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      "ALTER TABLE `collection` DROP FOREIGN KEY `FK_2aba6966a36688d00a10b49c904`",
      undefined
    );
    await queryRunner.query("DROP TABLE `calculation_rule`", undefined);
    await queryRunner.query(
      "ALTER TABLE `collection` DROP COLUMN `calculationRuleId`",
      undefined
    );
  }

  public async down(_: QueryRunner): Promise<any> {}
}
