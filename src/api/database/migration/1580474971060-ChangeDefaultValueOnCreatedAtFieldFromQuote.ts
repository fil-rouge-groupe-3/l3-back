import { MigrationInterface, QueryRunner } from "typeorm";

export class ChangeDefaultValueOnCreatedAtFieldFromQuote1580474971060
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      "ALTER TABLE `quote` DROP FOREIGN KEY `FK_343b48a78b7b5088a1f517d3ec5`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `quote` DROP COLUMN `createdAt`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `quote` ADD `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `quote` CHANGE `updatedAt` `updatedAt` varchar(255) NULL",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `quote` ADD CONSTRAINT `FK_d69dea9d1ca2816e94cd007a1a7` FOREIGN KEY (`quoteStateId`) REFERENCES `quote_state`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION",
      undefined
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      "ALTER TABLE `quote` DROP FOREIGN KEY `FK_d69dea9d1ca2816e94cd007a1a7`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `quote` CHANGE `updatedAt` `updatedAt` varchar(255) NOT NULL",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `quote` DROP COLUMN `createdAt`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `quote` ADD `createdAt` varchar(255) NOT NULL DEFAULT ''01/30/2020''",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `quote` ADD CONSTRAINT `FK_343b48a78b7b5088a1f517d3ec5` FOREIGN KEY (`quoteStateId`) REFERENCES `quote_state`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION",
      undefined
    );
  }
}
