import { MigrationInterface, QueryRunner } from "typeorm";

export class AddDependenciesOnModule1577445269253
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      "CREATE TABLE `module_section` (`id` int NOT NULL AUTO_INCREMENT, `name` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB",
      undefined
    );
    await queryRunner.query(
      "CREATE TABLE `module_rising` (`id` int NOT NULL AUTO_INCREMENT, `name` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB",
      undefined
    );
    await queryRunner.query(
      "CREATE TABLE `module_filling_between_rising` (`id` int NOT NULL AUTO_INCREMENT, `name` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `module` ADD `moduleSectionId` int NULL",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `module` ADD `moduleRisingId` int NULL",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `module` ADD `moduleFillingBetweenRisingId` int NULL",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `module` ADD CONSTRAINT `FK_b15f54eb07861d3f937b7a1ee29` FOREIGN KEY (`moduleSectionId`) REFERENCES `module_section`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `module` ADD CONSTRAINT `FK_063f887ebc2d553f95a3bfa6b5a` FOREIGN KEY (`moduleRisingId`) REFERENCES `module_rising`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `module` ADD CONSTRAINT `FK_551ad404c26e0bd8463697cea8a` FOREIGN KEY (`moduleFillingBetweenRisingId`) REFERENCES `module_filling_between_rising`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION",
      undefined
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      "ALTER TABLE `module` DROP FOREIGN KEY `FK_551ad404c26e0bd8463697cea8a`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `module` DROP FOREIGN KEY `FK_063f887ebc2d553f95a3bfa6b5a`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `module` DROP FOREIGN KEY `FK_b15f54eb07861d3f937b7a1ee29`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `module` DROP COLUMN `moduleFillingBetweenRisingId`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `module` DROP COLUMN `moduleRisingId`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `module` DROP COLUMN `moduleSectionId`",
      undefined
    );
    await queryRunner.query(
      "DROP TABLE `module_filling_between_rising`",
      undefined
    );
    await queryRunner.query("DROP TABLE `module_rising`", undefined);
    await queryRunner.query("DROP TABLE `module_section`", undefined);
  }
}
