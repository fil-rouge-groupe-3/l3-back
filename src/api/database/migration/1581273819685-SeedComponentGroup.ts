import { MigrationInterface, QueryRunner, getRepository } from "typeorm";
import { ComponentGroup } from "../entities/components/componentGroup.entity";
import { ComponentGroupSeed } from "../seeders/componentGroup.seeder";

export class SeedComponentGroup1581273819685 implements MigrationInterface {
  public async up(_: QueryRunner): Promise<any> {
    await getRepository(ComponentGroup).save(ComponentGroupSeed);
  }

  public async down(_: QueryRunner): Promise<any> {}
}
