import { MigrationInterface, QueryRunner } from "typeorm";

export class RenameAndAddDependenciesOnModule1577444984041
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      "ALTER TABLE `component` DROP FOREIGN KEY `FK_100db4386e85a85e84c6c125275`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `component` DROP FOREIGN KEY `FK_10b45b92f377152d17843d6f3fa`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `component` DROP FOREIGN KEY `FK_ee5d6b1a01f9fa735d899967080`",
      undefined
    );
    await queryRunner.query(
      "CREATE TABLE `element_usage_unit` (`id` int NOT NULL AUTO_INCREMENT, `name` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB",
      undefined
    );
    await queryRunner.query(
      "CREATE TABLE `element_characteristic` (`id` int NOT NULL AUTO_INCREMENT, `name` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB",
      undefined
    );
    await queryRunner.query(
      "CREATE TABLE `element_nature` (`id` int NOT NULL AUTO_INCREMENT, `name` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `component` DROP COLUMN `componentUsageUnitId`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `component` DROP COLUMN `componentCharacteristicId`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `component` DROP COLUMN `componentNatureId`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `component` ADD `elementUsageUnitId` int NULL",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `component` ADD `elementCharacteristicId` int NULL",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `component` ADD `elementNatureId` int NULL",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `module` ADD `elementCharacteristicId` int NULL",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `module` ADD `elementNatureId` int NULL",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `module` ADD `elementUsageUnitId` int NULL",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `component` ADD CONSTRAINT `FK_d72c6aa454464e0cd515ac60766` FOREIGN KEY (`elementUsageUnitId`) REFERENCES `element_usage_unit`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `component` ADD CONSTRAINT `FK_0f3c6d7b7e5d69914de27dbc11b` FOREIGN KEY (`elementCharacteristicId`) REFERENCES `element_characteristic`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `component` ADD CONSTRAINT `FK_351d5d5c07e6144e66d5d08abfb` FOREIGN KEY (`elementNatureId`) REFERENCES `element_nature`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `module` ADD CONSTRAINT `FK_3d4e6ebfbfc654f556cdb899365` FOREIGN KEY (`elementCharacteristicId`) REFERENCES `element_characteristic`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `module` ADD CONSTRAINT `FK_ca9b66436ede2c341cba35dcb42` FOREIGN KEY (`elementNatureId`) REFERENCES `element_nature`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `module` ADD CONSTRAINT `FK_b0a999c5ececa627b7a5a5925b3` FOREIGN KEY (`elementUsageUnitId`) REFERENCES `element_usage_unit`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION",
      undefined
    );
    await queryRunner.query("DROP TABLE `component_nature`", undefined);
    await queryRunner.query("DROP TABLE `component_characteristic`", undefined);
    await queryRunner.query("DROP TABLE `component_usage_unit`", undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      "CREATE TABLE `component_usage_unit` (`id` int NOT NULL AUTO_INCREMENT, `name` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB",
      undefined
    );
    await queryRunner.query(
      "CREATE TABLE `component_characteristic` (`id` int NOT NULL AUTO_INCREMENT, `name` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB",
      undefined
    );
    await queryRunner.query(
      "CREATE TABLE `component_nature` (`id` int NOT NULL AUTO_INCREMENT, `name` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `module` DROP FOREIGN KEY `FK_b0a999c5ececa627b7a5a5925b3`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `module` DROP FOREIGN KEY `FK_ca9b66436ede2c341cba35dcb42`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `module` DROP FOREIGN KEY `FK_3d4e6ebfbfc654f556cdb899365`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `component` DROP FOREIGN KEY `FK_351d5d5c07e6144e66d5d08abfb`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `component` DROP FOREIGN KEY `FK_0f3c6d7b7e5d69914de27dbc11b`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `component` DROP FOREIGN KEY `FK_d72c6aa454464e0cd515ac60766`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `module` DROP COLUMN `elementUsageUnitId`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `module` DROP COLUMN `elementNatureId`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `module` DROP COLUMN `elementCharacteristicId`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `component` DROP COLUMN `elementNatureId`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `component` DROP COLUMN `elementCharacteristicId`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `component` DROP COLUMN `elementUsageUnitId`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `component` ADD `componentNatureId` int NULL",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `component` ADD `componentCharacteristicId` int NULL",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `component` ADD `componentUsageUnitId` int NULL",
      undefined
    );
    await queryRunner.query("DROP TABLE `element_nature`", undefined);
    await queryRunner.query("DROP TABLE `element_characteristic`", undefined);
    await queryRunner.query("DROP TABLE `element_usage_unit`", undefined);
    await queryRunner.query(
      "ALTER TABLE `component` ADD CONSTRAINT `FK_ee5d6b1a01f9fa735d899967080` FOREIGN KEY (`componentCharacteristicId`) REFERENCES `component_characteristic`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `component` ADD CONSTRAINT `FK_10b45b92f377152d17843d6f3fa` FOREIGN KEY (`componentUsageUnitId`) REFERENCES `component_usage_unit`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `component` ADD CONSTRAINT `FK_100db4386e85a85e84c6c125275` FOREIGN KEY (`componentNatureId`) REFERENCES `component_nature`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION",
      undefined
    );
  }
}
