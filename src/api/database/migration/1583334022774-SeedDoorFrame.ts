import { MigrationInterface, QueryRunner, getRepository } from "typeorm";
import { DoorFrame } from "../entities/doorFrame.entity";
import { DoorFrameSeed } from "../seeders/doorFrame.seeder";

export class SeedDoorFrame1583334022774 implements MigrationInterface {
  public async up(_: QueryRunner): Promise<any> {
    await getRepository(DoorFrame).save(DoorFrameSeed);
  }

  public async down(_: QueryRunner): Promise<any> {}
}
