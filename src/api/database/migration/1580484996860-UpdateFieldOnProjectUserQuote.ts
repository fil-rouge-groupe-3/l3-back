import { MigrationInterface, QueryRunner } from "typeorm";

export class UpdateFieldOnProjectUserQuote1580484996860
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      "ALTER TABLE `project` ADD `updatedAt` timestamp NULL",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `project` ADD `startedAt` timestamp NULL",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `project` ADD `endedAt` timestamp NULL",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `project` DROP COLUMN `createdAt`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `project` ADD `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `quote` CHANGE `createdAt` `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP",
      undefined
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      "ALTER TABLE `quote` CHANGE `createdAt` `createdAt` timestamp NOT NULL DEFAULT 'current_timestamp()'",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `project` DROP COLUMN `createdAt`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `project` ADD `createdAt` varchar(255) NOT NULL",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `project` DROP COLUMN `updatedAt`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `project` DROP COLUMN `startedAt`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `project` DROP COLUMN `endedAt`",
      undefined
    );
  }
}
