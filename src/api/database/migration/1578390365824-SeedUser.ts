import { MigrationInterface, QueryRunner, getRepository } from "typeorm";
import { User } from "../entities/user.entity";
import { getUserSeeder } from "../seeders/user.seeder";

export class SeedUser1578390365824 implements MigrationInterface {
  public async up(_: QueryRunner): Promise<any> {
    await getRepository(User).save(getUserSeeder(50));
  }

  public async down(_: QueryRunner): Promise<any> {}
}
