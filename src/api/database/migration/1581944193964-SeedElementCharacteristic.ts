import { MigrationInterface, QueryRunner, getRepository } from "typeorm";
import { ElementCharacteristic } from "../entities/elementCharacteristic.entity";
import { ElementCharacteristicSeed } from "../seeders/entityCharacteristic.seeder";

export class SeedElementCharacteristic1581944193964
  implements MigrationInterface {
  public async up(_: QueryRunner): Promise<any> {
    await getRepository(ElementCharacteristic).save(ElementCharacteristicSeed);
  }

  public async down(_: QueryRunner): Promise<any> {}
}
