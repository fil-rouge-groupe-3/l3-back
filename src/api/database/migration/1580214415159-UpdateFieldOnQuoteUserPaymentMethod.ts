import { MigrationInterface, QueryRunner } from "typeorm";

export class UpdateFieldOnQuoteUserPaymentMethod1580214415159
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      "ALTER TABLE `payment_method` DROP COLUMN `step`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `quote` DROP COLUMN `amoutTtc`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `payment_method` ADD `stepName` varchar(255) NOT NULL",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `payment_method` ADD `description` varchar(255) NOT NULL",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `quote_state` ADD `description` varchar(255) NOT NULL",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `quote` ADD `createdAt` varchar(255) NOT NULL DEFAULT '01/28/2020'",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `quote` ADD `updatedAt` timestamp NULL",
      undefined
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      "ALTER TABLE `quote` DROP COLUMN `updatedAt`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `quote` DROP COLUMN `createdAt`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `quote_state` DROP COLUMN `description`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `payment_method` DROP COLUMN `description`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `payment_method` DROP COLUMN `stepName`",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `quote` ADD `amoutTtc` int NOT NULL",
      undefined
    );
    await queryRunner.query(
      "ALTER TABLE `payment_method` ADD `step` varchar(255) NOT NULL",
      undefined
    );
  }
}
