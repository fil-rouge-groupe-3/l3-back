import { MigrationInterface, QueryRunner, getRepository } from "typeorm";
import { Coverage } from "../entities/coverage.entity";
import { CoverageSeed } from "../seeders/coverage.seeder";

export class SeedCoverage1583336416771 implements MigrationInterface {
  public async up(_: QueryRunner): Promise<any> {
    await getRepository(Coverage).save(CoverageSeed);
  }

  public async down(_: QueryRunner): Promise<any> {}
}
