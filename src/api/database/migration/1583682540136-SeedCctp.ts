import {MigrationInterface, QueryRunner, getRepository} from "typeorm";
import { Cctp } from "../entities/cctp.entity";
import { CctpSeed } from "../seeders/cctp.seeder";

export class SeedCctp1583682540136 implements MigrationInterface {

    public async up(_: QueryRunner): Promise<any> {
        await getRepository(Cctp).save(CctpSeed);
    }

    public async down(_: QueryRunner): Promise<any> {
    }

}
