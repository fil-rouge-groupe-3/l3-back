import { MigrationInterface, QueryRunner, getRepository } from "typeorm";
import { Component } from "../entities/components/component.entity";
import { getComponentSeeder } from "../seeders/component.seeder";

export class SeedComponent1582024039910 implements MigrationInterface {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  public async up(_: QueryRunner): Promise<any> {
    await getRepository(Component).save(getComponentSeeder());
  }

  public async down(_: QueryRunner): Promise<any> {}
}
