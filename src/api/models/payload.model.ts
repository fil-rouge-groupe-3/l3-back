export interface IPayload {
  id: number;
  email: string;
  avatar: string;
  rank: number;
}
