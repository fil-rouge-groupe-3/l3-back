import { Request, Response, NextFunction } from "express";
import jwt = require("jsonwebtoken");

export const jwtMiddleware = (
  req: Request,
  res: Response,
  next: NextFunction,
  rank = 0
) => {
  let token: string = req.headers["authorization"];

  if (token) {
    token = token.replace("Bearer ", "");

    jwt.verify(
      token,
      process.env.JWT_SECRET as string,
      (err: jwt.VerifyErrors, decoded: string | object) => {
        if (err) {
          return res.status(401).json({ message: "invalid_token" });
        } else {
          req["decoded"] = decoded;
          if (rank != 0) {
            if (req["decoded"].rank != 1) {
              if (req["decoded"].rank != rank) {
                return res.status(401).send({
                  message: "invalid_rank",
                  actual_rank: req["decoded"].rank,
                  expected_rank: rank
                });
              }
              next();
              return;
            }
            next();
            return;
          }
          next();
          return;
        }
      }
    );
  } else {
    res.status(401).send({ message: "no_token_provided" });
  }
};
