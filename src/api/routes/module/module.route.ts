import { Router } from "express";
import ModuleController from "../../controllers/module.controller";
import { Module } from "../../database/entities/modules/module.entity";
import { jwtMiddleware } from "../../middlewares/jwt.middleware";

const routes = Router();
const moduleController = new ModuleController();

// Global

routes
  .route("/")
  .get(
    (req, res, next) => jwtMiddleware(req, res, next),
    (req, res) =>
      moduleController.getAll(
        req,
        res,
        Module,
        ["id", "name", "lenght", "width", "principleCut"],
        [
          "cctp",
          "moduleFillingBetweenRising",
          "moduleRising",
          "moduleSection",
          "elementUsageUnit",
          "elementCharacteristic",
          "elementNature",
          "collections",
          "components",
          "components.elementNature"
        ]
      )
  )
  .post(
    (req, res, next) => jwtMiddleware(req, res, next),
    (req, res) => moduleController.post(req, res, Module)
  );

// With Id

routes
  .route("/:id([0-9]+)")
  .get(
    (req, res, next) => jwtMiddleware(req, res, next),
    (req, res) =>
      moduleController.getOneById(
        req,
        res,
        Module,
        ["id", "name", "lenght", "width", "principleCut"],
        [
          "cctp",
          "moduleFillingBetweenRising",
          "moduleRising",
          "moduleSection",
          "elementUsageUnit",
          "elementCharacteristic",
          "elementNature",
          "collections",
          "components",
          "components.elementNature"
        ]
      )
  )
  .put(
    (req, res, next) => jwtMiddleware(req, res, next),
    (req, res) => moduleController.put(req, res, Module)
  )
  .delete(
    (req, res, next) => jwtMiddleware(req, res, next),
    (req, res) => moduleController.delete(req, res, Module)
  );

export default routes;
