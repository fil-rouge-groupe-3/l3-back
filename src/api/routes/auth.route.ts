import { Router } from "express";
import AuthController from "../controllers/auth.controller";

const routes = Router();
const authController = new AuthController();

// Global

routes.route("/register").post(authController.register);

/**
 * @swagger
 * /auth/login:
 *  $ref: "#/definitions/AuthGlobal"
 */
routes.route("/login").post(authController.login);

routes.route("/forgot-password").put(authController.forgotPassword);

export default routes;
