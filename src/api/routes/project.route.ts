import { Router } from "express";
import ProjectController from "../controllers/project.controller";
import { Project } from "../database/entities/project.entity";

const routes = Router();
const projectController = new ProjectController();

// Global

routes
  .route("/")
  .get((req, res) =>
    projectController.getAll(
      req,
      res,
      Project,
      [
        "id",
        "ref",
        "name",
        "desc",
        "startedAt",
        "endedAt",
        "createdAt",
        "updatedAt"
      ],
      ["users", "customers", "quotes", "quotes.paymentMethod", "quotes.quoteState", "quotes.crossSection", "users.userRole"]
    )
  )
  .post((req, res) => projectController.post(req, res, Project));

// With Id

routes
  .route("/:id([0-9]+)")
  .get((req, res) =>
    projectController.getOneById(
      req,
      res,
      Project,
      [
        "id",
        "ref",
        "name",
        "desc",
        "startedAt",
        "endedAt",
        "createdAt",
        "updatedAt"
      ],
      ["users", "customers", "quotes", "quotes.paymentMethod", "quotes.quoteState", "quotes.crossSection", "users.userRole"]
    )
  )
  .put((req, res) => projectController.putCustom(req, res))
  .delete((req, res) => projectController.delete(req, res, Project));

export default routes;
