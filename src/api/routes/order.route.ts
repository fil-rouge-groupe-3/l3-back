import { Router } from "express";
import OrderController from "../controllers/order.controller";
import { Order } from "../database/entities/order.entity";

const routes = Router();
const orderController = new OrderController();

// Global

routes
  .route("/")
  .get((req, res) =>
    orderController.getAll(
      req,
      res,
      Order,
      ["id"],
      ["quote", "component", "component.elementNature"]
    )
  )
  .post((req, res) => orderController.post(req, res, Order));

// With Id

routes
  .route("/:id([0-9]+)")
  .get((req, res) =>
    orderController.getOneById(
      req,
      res,
      Order,
      ["id"],
      ["quote", "component", "component.elementNature"]
    )
  )
  .put((req, res) => orderController.put(req, res, Order))
  .delete((req, res) => orderController.delete(req, res, Order));

export default routes;
