import { Router } from "express";
import ProviderController from "../controllers/provider.controller";
import { Provider } from "../database/entities/provider.entity";
import { jwtMiddleware } from "../middlewares/jwt.middleware";

const routes = Router();
const providerController = new ProviderController();

// Global

routes
  .route("/")
  .get(
    (req, res, next) => jwtMiddleware(req, res, next, 2),
    (req, res) =>
      providerController.getAll(req, res, Provider, [
        "id",
        "name",
        "tel",
        "website",
        "email",
        "address",
        "zipcode",
        "city",
        "country",
        "siret"
      ])
  )
  .post(
    (req, res, next) => jwtMiddleware(req, res, next, 2),
    (req, res) => providerController.post(req, res, Provider)
  );

// With Id

routes
  .route("/:id([0-9]+)")
  .get(
    (req, res, next) => jwtMiddleware(req, res, next, 2),
    (req, res) =>
      providerController.getOneById(req, res, Provider, [
        "id",
        "name",
        "tel",
        "website",
        "email",
        "address",
        "zipcode",
        "city",
        "country",
        "siret"
      ])
  )
  .put(
    (req, res, next) => jwtMiddleware(req, res, next, 2),
    (req, res) => providerController.put(req, res, Provider)
  )
  .delete(
    (req, res, next) => jwtMiddleware(req, res, next, 2),
    (req, res) => providerController.delete(req, res, Provider)
  );

export default routes;
