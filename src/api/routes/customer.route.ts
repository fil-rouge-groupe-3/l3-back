import { Router } from "express";
import CustomerController from "../controllers/customer.controller";
import { Customer } from "../database/entities/customer.entity";

const routes = Router();
const customerController = new CustomerController();

// Global

routes
  .route("/")
  .get((req, res) =>
    customerController.getAll(
      req,
      res,
      Customer,
      [
        "id",
        "firstName",
        "lastName",
        "company",
        "siret",
        "address",
        "zipcode",
        "tel"
      ],
      ["projects"]
    )
  )
  .post((req, res) => customerController.post(req, res, Customer));

// With Id

routes
  .route("/:id([0-9]+)")
  .get((req, res) =>
    customerController.getOneById(
      req,
      res,
      Customer,
      [
        "id",
        "firstName",
        "lastName",
        "company",
        "siret",
        "address",
        "zipcode",
        "tel"
      ],
      ["projects"]
    )
  )
  .put((req, res) => customerController.put(req, res, Customer))
  .delete((req, res) => customerController.delete(req, res, Customer));

export default routes;
