import { Router } from "express";
import ComponentGroupController from "../../controllers/componentGroup.controller";
import { ComponentGroup } from "../../database/entities/components/componentGroup.entity";
import { jwtMiddleware } from "../../middlewares/jwt.middleware";

const routes = Router();
const componentGroupController = new ComponentGroupController();

// Global

routes
  .route("/")
  .get(
    (req, res, next) => jwtMiddleware(req, res, next, 2),
    (req, res) =>
      componentGroupController.getAll(req, res, ComponentGroup, ["id", "name"])
  )
  .post(
    (req, res, next) => jwtMiddleware(req, res, next, 2),
    (req, res) => componentGroupController.post(req, res, ComponentGroup)
  );

// With Id

routes
  .route("/:id([0-9]+)")
  .get(
    (req, res, next) => jwtMiddleware(req, res, next, 2),
    (req, res) =>
      componentGroupController.getOneById(req, res, ComponentGroup, [
        "id",
        "name"
      ])
  )
  .put(
    (req, res, next) => jwtMiddleware(req, res, next, 2),
    (req, res) => componentGroupController.put(req, res, ComponentGroup)
  )
  .delete(
    (req, res, next) => jwtMiddleware(req, res, next, 2),
    (req, res) => componentGroupController.delete(req, res, ComponentGroup)
  );

export default routes;
