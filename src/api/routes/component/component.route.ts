import ComponentController from "../../controllers/component.controller";
import { Router } from "express";
import { Component } from "../../database/entities/components/component.entity";
import { jwtMiddleware } from "../../middlewares/jwt.middleware";

const routes = Router();
const componentController = new ComponentController();

// Global

routes
  .route("/")
  .get(
    (req, res, next) => jwtMiddleware(req, res, next),
    (req, res) =>
      componentController.getAll(
        req,
        res,
        Component,
        ["id", "priceHt"],
        [
          "elementUsageUnit",
          "componentGroup",
          "elementCharacteristic",
          "elementNature",
          "provider",
          "orders"
        ]
      )
  )
  .post(
    (req, res, next) => jwtMiddleware(req, res, next, 2),
    (req, res) => componentController.post(req, res, Component)
  );

// With Id

routes
  .route("/:id([0-9]+)")
  .get(
    (req, res, next) => jwtMiddleware(req, res, next),
    (req, res) =>
      componentController.getOneById(
        req,
        res,
        Component,
        ["id", "priceHt"],
        [
          "elementUsageUnit",
          "componentGroup",
          "elementCharacteristic",
          "elementNature",
          "provider",
          "orders"
        ]
      )
  )
  .put(
    (req, res, next) => jwtMiddleware(req, res, next, 2),
    (req, res) => componentController.put(req, res, Component)
  )
  .delete(
    (req, res, next) => jwtMiddleware(req, res, next, 2),
    (req, res) => componentController.delete(req, res, Component)
  );

export default routes;
