import { Router } from "express";
import auth from "./auth.route";
import user from "./user/user.route";
import userRole from "./user/userRole.route";
import floor from "./collection/floor.route";
import coverage from "./collection/coverage.route";
import doorFrame from "./collection/doorFrame.route";
import cctp from "./collection/cctp.route";
import collection from "./collection/collection.route";
import crossSection from "./crossSection.route";
import elementUsageUnit from "./element/elementUsageUnit.route";
import elementCharacteristic from "./element/elementCharacteristic.route";
import elementNature from "./element/elementNature.route";
import component from "./component/component.route";
import modulebase from "./module/module.route";
import customer from "./customer.route";
import project from "./project.route";
import order from "./order.route";
import provider from "./provider.route";
import componentGroup from "./component/componentGroup.route";
import quote from "./quote/quote.route";
import quoteState from "./quote/quoteState.route";
import paymentMethod from "./quote/paymentMethod.route";

const router = Router();

/**
 * @swagger
 * /:
 *  get:
 *    summary: Vérifie si le status de l'API est "OK"
 *    responses:
 *      '200':
 *        description: l'API est bien lancé
 */
router.get("/", (_req, res, _next) => {
  res.send("OK");
});

router.use("/auth", auth);

/**
 * @swagger
 * /users:
 *  $ref: "#/definitions/UserGlobal"
 * /users/me:
 *  $ref: "#/definitions/UserCurrent"
 * /users/{id}:
 *  $ref: "#/definitions/UserById"
 */
router.use("/users", user);

/**
 * @swagger
 * /user-roles:
 *  $ref: "#/definitions/UserRoleGlobal"
 * /user-roles/{id}:
 *  $ref: "#/definitions/UserRoleById"
 */
router.use("/user-roles", userRole);
router.use("/floors", floor);
router.use("/door-frames", doorFrame);
router.use("/cctps", cctp);
router.use("/coverages", coverage);
router.use("/collections", collection);

/**
 * @swagger
 * /cross-sections:
 *  $ref: "#/definitions/CrossSectionGlobal"
 * /cross-sections/{id}:
 *  $ref: "#/definitions/CrossSectionById"
 */
router.use("/cross-sections", crossSection);

/**
 * @swagger
 * /payment-methods:
 *  $ref: "#/definitions/PaymentMethodGlobal"
 * /payment-methods/{id}:
 *  $ref: "#/definitions/PaymentMethodById"
 */
router.use("/payment-methods", paymentMethod);

/**
 * @swagger
 * /components:
 *  $ref: "#/definitions/ComponentGlobal"
 * /components/{id}:
 *  $ref: "#/definitions/ComponentById"
 */
router.use("/components", component);

/**
 * @swagger
 * /element-characteristics:
 *  $ref: "#/definitions/ElementCharacteristicGlobal"
 * /element-characteristics/{id}:
 *  $ref: "#/definitions/ElementCharacteristicById"
 */
router.use("/element-characteristics", elementCharacteristic);

/**
 * @swagger
 * /element-natures:
 *  $ref: "#/definitions/ElementNatureGlobal"
 * /element-natures/{id}:
 *  $ref: "#/definitions/ElementNatureById"
 */
router.use("/element-natures", elementNature);

/**
 * @swagger
 * /element-usage-units:
 *  $ref: "#/definitions/ElementUsageUnitGlobal"
 * /element-usage-units/{id}:
 *  $ref: "#/definitions/ElementUsageUnitById"
 */
router.use("/element-usage-units", elementUsageUnit);

/**
 * @swagger
 * /customers:
 *  $ref: "#/definitions/CustomerGlobal"
 * /customers/{id}:
 *  $ref: "#/definitions/CustomerById"
 */
router.use("/customers", customer);
router.use("/modules", modulebase);

/**
 * @swagger
 * /projects:
 *  $ref: "#/definitions/ProjectGlobal"
 * /projects/{id}:
 *  $ref: "#/definitions/ProjectById"
 */
router.use("/projects", project);
router.use("/orders", order);

/**
 * @swagger
 * /quotes:
 *  $ref: "#/definitions/QuoteGlobal"
 * /quotes/{id}:
 *  $ref: "#/definitions/QuoteById"
 */
router.use("/quotes", quote);

/**
 * @swagger
 * /quote-states:
 *  $ref: "#/definitions/QuoteStateGlobal"
 * /quote-states/{id}:
 *  $ref: "#/definitions/QuoteStateById"
 */
router.use("/quote-states", quoteState);

/**
 * @swagger
 * /providers:
 *  $ref: "#/definitions/ProviderGlobal"
 * /providers/{id}:
 *  $ref: "#/definitions/ProviderById"
 */
router.use("/providers", provider);

/**
 * @swagger
 * /component-groups:
 *  $ref: "#/definitions/ComponentGroupGlobal"
 * /component-groups/{id}:
 *  $ref: "#/definitions/ComponentGroupById"
 */
router.use("/component-groups", componentGroup);

export default router;
