import { Coverage } from "../../database/entities/coverage.entity";
import { Router } from "express";
import CoverageController from "../../controllers/coverage.controller";
import { jwtMiddleware } from "../../middlewares/jwt.middleware";

const routes = Router();
const coverageController = new CoverageController();

// Global

routes
  .route("/")
  .get(
    (req, res, next) => jwtMiddleware(req, res, next),
    (req, res) =>
      coverageController.getAll(
        req,
        res,
        Coverage,
        ["id", "name", "overview", "priceHt"]
      )
  )
  .post(
    (req, res, next) => jwtMiddleware(req, res, next, 2),
    (req, res) => coverageController.post(req, res, Coverage)
  );

// With Id

routes
  .route("/:id([0-9]+)")
  .get(
    (req, res, next) => jwtMiddleware(req, res, next),
    (req, res) =>
      coverageController.getOneById(
        req,
        res,
        Coverage,
        ["id", "name", "overview", "priceHt"]
      )
  )
  .put(
    (req, res, next) => jwtMiddleware(req, res, next, 2),
    (req, res) => coverageController.put(req, res, Coverage)
  )
  .delete(
    (req, res, next) => jwtMiddleware(req, res, next, 2),
    (req, res) => coverageController.delete(req, res, Coverage)
  );

export default routes;
