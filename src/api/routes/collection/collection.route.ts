import { Collection } from "../../database/entities/collection.entity";
import { Router } from "express";
import CollectionController from "../../controllers/collection.controller";
import { jwtMiddleware } from "../../middlewares/jwt.middleware";

const routes = Router();
const collectionController = new CollectionController();

// Global

routes
  .route("/")
  .get(
    (req, res, next) => jwtMiddleware(req, res, next),
    (req, res) =>
      collectionController.getAll(
        req,
        res,
        Collection,
        ["id", "name"],
        ["crossSection", "doorFrame", "floor", "coverage", "modules", "cctp"]
      )
  )
  .post(
    (req, res, next) => jwtMiddleware(req, res, next, 2),
    (req, res) => collectionController.post(req, res, Collection)
  );

// With Id

routes
  .route("/:id([0-9]+)")
  .get(
    (req, res, next) => jwtMiddleware(req, res, next),
    (req, res) =>
      collectionController.getOneById(
        req,
        res,
        Collection,
        ["id", "name"],
        ["crossSection", "doorFrame", "floor", "coverage", "modules", "cctp"]
      )
  )
  .put(
    (req, res, next) => jwtMiddleware(req, res, next, 2),
    (req, res) => collectionController.put(req, res, Collection)
  )
  .delete(
    (req, res, next) => jwtMiddleware(req, res, next, 2),
    (req, res) => collectionController.delete(req, res, Collection)
  );

export default routes;
