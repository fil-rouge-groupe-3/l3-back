import { DoorFrame } from "../../database/entities/doorFrame.entity";
import { Router } from "express";
import DoorFrameController from "../../controllers/doorFrame.controller";
import { jwtMiddleware } from "../../middlewares/jwt.middleware";

const routes = Router();
const doorFrameController = new DoorFrameController();

// Global

routes
  .route("/")
  .get(
    (req, res, next) => jwtMiddleware(req, res, next),
    (req, res) =>
      doorFrameController.getAll(
        req,
        res,
        DoorFrame,
        ["id", "name", "quality"]
      )
  )
  .post(
    (req, res, next) => jwtMiddleware(req, res, next, 2),
    (req, res) => doorFrameController.post(req, res, DoorFrame)
  );

// With Id

routes
  .route("/:id([0-9]+)")
  .get(
    (req, res, next) => jwtMiddleware(req, res, next),
    (req, res) =>
      doorFrameController.getOneById(
        req,
        res,
        DoorFrame,
        ["id", "name", "quality"]
      )
  )
  .put(
    (req, res, next) => jwtMiddleware(req, res, next, 2),
    (req, res) => doorFrameController.put(req, res, DoorFrame)
  )
  .delete(
    (req, res, next) => jwtMiddleware(req, res, next, 2),
    (req, res) => doorFrameController.delete(req, res, DoorFrame)
  );

export default routes;
