import { Cctp } from "../../database/entities/cctp.entity";
import { Router } from "express";
import CctpController from "../../controllers/cctp.controller";
import { jwtMiddleware } from "../../middlewares/jwt.middleware";

const routes = Router();
const cctpController = new CctpController();

// Global

routes
  .route("/")
  .get(
    (req, res, next) => jwtMiddleware(req, res, next),
    (req, res) =>
      cctpController.getAll(
        req,
        res,
        Cctp,
        ["id", "name"]
      )
  )
  .post(
    (req, res, next) => jwtMiddleware(req, res, next, 2),
    (req, res) => cctpController.post(req, res, Cctp)
  );

// With Id

routes
  .route("/:id([0-9]+)")
  .get(
    (req, res, next) => jwtMiddleware(req, res, next),
    (req, res) =>
      cctpController.getOneById(
        req,
        res,
        Cctp,
        ["id", "name"]
      )
  )
  .put(
    (req, res, next) => jwtMiddleware(req, res, next, 2),
    (req, res) => cctpController.put(req, res, Cctp)
  )
  .delete(
    (req, res, next) => jwtMiddleware(req, res, next, 2),
    (req, res) => cctpController.delete(req, res, Cctp)
  );

export default routes;
