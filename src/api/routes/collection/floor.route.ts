import { Floor } from "../../database/entities/floor.entity";
import { Router } from "express";
import FloorController from "../../controllers/floor.controller";
import { jwtMiddleware } from "../../middlewares/jwt.middleware";

const routes = Router();
const floorController = new FloorController();

// Global

routes
  .route("/")
  .get(
    (req, res, next) => jwtMiddleware(req, res, next),
    (req, res) =>
      floorController.getAll(
        req,
        res,
        Floor,
        ["id", "name", "overview", "priceHt"]
      )
  )
  .post(
    (req, res, next) => jwtMiddleware(req, res, next, 2),
    (req, res) => floorController.post(req, res, Floor)
  );

// With Id

routes
  .route("/:id([0-9]+)")
  .get(
    (req, res, next) => jwtMiddleware(req, res, next),
    (req, res) =>
      floorController.getOneById(
        req,
        res,
        Floor,
        ["id", "name", "overview", "priceHt"]
      )
  )
  .put(
    (req, res, next) => jwtMiddleware(req, res, next, 2),
    (req, res) => floorController.put(req, res, Floor)
  )
  .delete(
    (req, res, next) => jwtMiddleware(req, res, next, 2),
    (req, res) => floorController.delete(req, res, Floor)
  );

export default routes;
