import { Router } from "express";
import { jwtMiddleware } from "../../middlewares/jwt.middleware";
import { ElementCharacteristic } from "../../database/entities/elementCharacteristic.entity";
import ElementCharacteristicController from "../../controllers/elementCharacteristic.controller";

const routes = Router();
const elementCharacteristicController = new ElementCharacteristicController();

// Global

routes
  .route("/")
  .get(
    (req, res, next) => jwtMiddleware(req, res, next, 2),
    (req, res) =>
      elementCharacteristicController.getAll(req, res, ElementCharacteristic, [
        "id",
        "name"
      ])
  )
  .post(
    (req, res, next) => jwtMiddleware(req, res, next, 2),
    (req, res) =>
      elementCharacteristicController.post(req, res, ElementCharacteristic)
  );

// With Id

routes
  .route("/:id([0-9]+)")
  .get(
    (req, res, next) => jwtMiddleware(req, res, next, 2),
    (req, res) =>
      elementCharacteristicController.getOneById(
        req,
        res,
        ElementCharacteristic,
        ["id", "name"]
      )
  )
  .put(
    (req, res, next) => jwtMiddleware(req, res, next, 2),
    (req, res) =>
      elementCharacteristicController.put(req, res, ElementCharacteristic)
  )
  .delete(
    (req, res, next) => jwtMiddleware(req, res, next, 2),
    (req, res) =>
      elementCharacteristicController.delete(req, res, ElementCharacteristic)
  );

export default routes;
