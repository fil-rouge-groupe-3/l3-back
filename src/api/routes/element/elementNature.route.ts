import { Router } from "express";
import { jwtMiddleware } from "../../middlewares/jwt.middleware";
import ElementNatureController from "../../controllers/elementNature.controller";
import { ElementNature } from "../../database/entities/elementNature.entity";

const routes = Router();
const elementNatureController = new ElementNatureController();

// Global

routes
  .route("/")
  .get(
    (req, res, next) => jwtMiddleware(req, res, next, 2),
    (req, res) =>
      elementNatureController.getAll(req, res, ElementNature, ["id", "name"])
  )
  .post(
    (req, res, next) => jwtMiddleware(req, res, next, 2),
    (req, res) => elementNatureController.post(req, res, ElementNature)
  );

// With Id

routes
  .route("/:id([0-9]+)")
  .get(
    (req, res, next) => jwtMiddleware(req, res, next, 2),
    (req, res) =>
      elementNatureController.getOneById(req, res, ElementNature, [
        "id",
        "name"
      ])
  )
  .put(
    (req, res, next) => jwtMiddleware(req, res, next, 2),
    (req, res) => elementNatureController.put(req, res, ElementNature)
  )
  .delete(
    (req, res, next) => jwtMiddleware(req, res, next, 2),
    (req, res) => elementNatureController.delete(req, res, ElementNature)
  );

export default routes;
