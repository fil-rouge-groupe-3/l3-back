import { Router } from "express";
import { jwtMiddleware } from "../../middlewares/jwt.middleware";
import { ElementUsageUnit } from "../../database/entities/elementUsageUnit.entity";
import ElementUsageUnitController from "../../controllers/elementUsageUnit.controller";

const routes = Router();
const elementUsageUnitController = new ElementUsageUnitController();

// Global

routes
  .route("/")
  .get(
    (req, res, next) => jwtMiddleware(req, res, next, 2),
    (req, res) =>
      elementUsageUnitController.getAll(req, res, ElementUsageUnit, [
        "id",
        "name"
      ])
  )
  .post(
    (req, res, next) => jwtMiddleware(req, res, next, 2),
    (req, res) => elementUsageUnitController.post(req, res, ElementUsageUnit)
  );

// With Id

routes
  .route("/:id([0-9]+)")
  .get(
    (req, res, next) => jwtMiddleware(req, res, next, 2),
    (req, res) =>
      elementUsageUnitController.getOneById(req, res, ElementUsageUnit, [
        "id",
        "name"
      ])
  )
  .put(
    (req, res, next) => jwtMiddleware(req, res, next, 2),
    (req, res) => elementUsageUnitController.put(req, res, ElementUsageUnit)
  )
  .delete(
    (req, res, next) => jwtMiddleware(req, res, next, 2),
    (req, res) => elementUsageUnitController.delete(req, res, ElementUsageUnit)
  );

export default routes;
