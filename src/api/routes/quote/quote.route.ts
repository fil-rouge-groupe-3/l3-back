import { Router } from "express";
import QuoteController from "../../controllers/quote.controller";
import { Quote } from "../../database/entities/quote.entity";
import { jwtMiddleware } from "../../middlewares/jwt.middleware";

const routes = Router();
const quoteController = new QuoteController();

// Global

routes
  .route("/")
  .get(
    (req, res, next) => jwtMiddleware(req, res, next),
    (req, res) =>
      quoteController.getAll(
        req,
        res,
        Quote,
        ["id", "name", "amoutHt", "createdAt", "updatedAt"],
        ["crossSection", "paymentMethod", "quoteState", "projects", "orders"]
      )
  )
  .post(
    (req, res, next) => jwtMiddleware(req, res, next),
    (req, res) => quoteController.post(req, res, Quote)
  );

// With Id

routes
  .route("/:id([0-9]+)")
  .get(
    (req, res, next) => jwtMiddleware(req, res, next),
    (req, res) =>
      quoteController.getOneById(
        req,
        res,
        Quote,
        ["id", "name", "amoutHt", "createdAt", "updatedAt"],
        ["crossSection", "paymentMethod", "quoteState", "projects", "orders"]
      )
  )
  .put(
    (req, res, next) => jwtMiddleware(req, res, next),
    (req, res) => quoteController.put(req, res, Quote)
  )
  .delete(
    (req, res, next) => jwtMiddleware(req, res, next),
    (req, res) => quoteController.delete(req, res, Quote)
  );

export default routes;
