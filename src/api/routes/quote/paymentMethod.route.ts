import { Router } from "express";
import { jwtMiddleware } from "../../middlewares/jwt.middleware";
import PaymentMethodController from "../../controllers/paymentMethod.controller";
import { PaymentMethod } from "../../database/entities/paymentMethod.entity";

const routes = Router();
const paymentMethodController = new PaymentMethodController();

// Global

routes
  .route("/")
  .get(
    (req, res, next) => jwtMiddleware(req, res, next),
    (req, res) =>
      paymentMethodController.getAll(
        req,
        res,
        PaymentMethod,
        ["id", "stepName", "description", "percentageToUnlock"],
        ["quotes"]
      )
  )
  .post(
    (req, res, next) => jwtMiddleware(req, res, next),
    (req, res) => paymentMethodController.post(req, res, PaymentMethod)
  );

// With Id

routes
  .route("/:id([0-9]+)")
  .get(
    (req, res, next) => jwtMiddleware(req, res, next),
    (req, res) =>
      paymentMethodController.getOneById(
        req,
        res,
        PaymentMethod,
        ["id", "stepName", "description", "percentageToUnlock"],
        ["quotes"]
      )
  )
  .put(
    (req, res, next) => jwtMiddleware(req, res, next),
    (req, res) => paymentMethodController.put(req, res, PaymentMethod)
  )
  .delete(
    (req, res, next) => jwtMiddleware(req, res, next),
    (req, res) => paymentMethodController.delete(req, res, PaymentMethod)
  );

export default routes;
