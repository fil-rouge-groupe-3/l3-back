import { Router } from "express";
import { jwtMiddleware } from "../../middlewares/jwt.middleware";
import QuoteStateController from "../../controllers/quoteState.controller";
import { QuoteState } from "../../database/entities/quoteState.entity";

const routes = Router();
const quoteStateController = new QuoteStateController();

// Global

routes
  .route("/")
  .get(
    (req, res, next) => jwtMiddleware(req, res, next),
    (req, res) =>
      quoteStateController.getAll(
        req,
        res,
        QuoteState,
        ["id", "name", "description", "color"],
        ["quotes"]
      )
  )
  .post(
    (req, res, next) => jwtMiddleware(req, res, next),
    (req, res) => quoteStateController.post(req, res, QuoteState)
  );

// With Id

routes
  .route("/:id([0-9]+)")
  .get(
    (req, res, next) => jwtMiddleware(req, res, next),
    (req, res) =>
      quoteStateController.getOneById(
        req,
        res,
        QuoteState,
        ["id", "name", "description", "color"],
        ["quotes"]
      )
  )
  .put(
    (req, res, next) => jwtMiddleware(req, res, next),
    (req, res) => quoteStateController.put(req, res, QuoteState)
  )
  .delete(
    (req, res, next) => jwtMiddleware(req, res, next),
    (req, res) => quoteStateController.delete(req, res, QuoteState)
  );

export default routes;
