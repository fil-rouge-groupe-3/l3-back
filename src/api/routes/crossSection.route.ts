import { Router } from "express";
import CrossSectionController from "../controllers/crossSection.controller";
import { CrossSection } from "../database/entities/crossSection.entity";

const routes = Router();
const crossSectionController = new CrossSectionController();

// Global

routes
  .route("/")
  .get((req, res) =>
    crossSectionController.getAll(
      req,
      res,
      CrossSection,
      ["id", "blueprintName", "blueprintPath"],
      ["quotes"]
    )
  )
  .post((req, res) => crossSectionController.post(req, res, CrossSection));

// With Id

routes
  .route("/:id([0-9]+)")
  .get((req, res) =>
    crossSectionController.getOneById(
      req,
      res,
      CrossSection,
      ["id", "blueprintName", "blueprintPath"],
      ["quotes"]
    )
  )
  .put((req, res) => crossSectionController.put(req, res, CrossSection))
  .delete((req, res) => crossSectionController.delete(req, res, CrossSection));

export default routes;
