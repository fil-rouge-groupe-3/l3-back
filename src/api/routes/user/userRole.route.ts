import { Router } from "express";
import { UserRoleController } from "../../controllers/userRole.controller";
import { UserRole } from "../../database/entities/userRole.entity";
import { jwtMiddleware } from "../../middlewares/jwt.middleware";

const routes = Router();
const userRoleController = new UserRoleController();

// Global

routes
  .route("/")
  .get(
    (req, res, next) => jwtMiddleware(req, res, next, 1),
    (req, res) =>
      userRoleController.getAll(
        req,
        res,
        UserRole,
        ["id", "name", "description"],
        ["users"]
      )
  )
  .post(
    (req, res, next) => jwtMiddleware(req, res, next, 1),
    (req, res) => userRoleController.post(req, res, UserRole)
  );

// With Id

routes
  .route("/:id([0-9]+)")
  .get(
    (req, res, next) => jwtMiddleware(req, res, next, 1),
    (req, res) =>
      userRoleController.getOneById(
        req,
        res,
        UserRole,
        ["id", "name", "description"],
        ["users"]
      )
  )
  .put(
    (req, res, next) => jwtMiddleware(req, res, next, 1),
    (req, res) => userRoleController.put(req, res, UserRole)
  )
  .delete(
    (req, res, next) => jwtMiddleware(req, res, next, 1),
    (req, res) => userRoleController.delete(req, res, UserRole)
  );

export default routes;
