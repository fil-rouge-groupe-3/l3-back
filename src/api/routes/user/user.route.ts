import { User } from "../../database/entities/user.entity";
import { Router } from "express";
import UserController from "../../controllers/user.controller";
import { jwtMiddleware } from "../../middlewares/jwt.middleware";

const routes = Router();
const userController = new UserController();

// Global

routes
  .route("/")
  .get(
    (req, res, next) => jwtMiddleware(req, res, next, 1),
    (req, res) =>
      userController.getAll(
        req,
        res,
        User,
        [
          "id",
          "email",
          "firstName",
          "lastName",
          "tel",
          "zipcode",
          "address",
          "city",
          "avatar_img"
        ],
        ["userRole", "projects"]
      )
  )
  .post(
    (req, res, next) => jwtMiddleware(req, res, next, 1),
    (req, res) => userController.post(req, res, User)
  );

routes
  .route("/me")
  .get((req, res) =>
    userController.getCurrentUser(
      req,
      res,
      [
        "id",
        "email",
        "firstName",
        "lastName",
        "tel",
        "zipcode",
        "address",
        "city",
        "avatar_img"
      ],
      ["userRole", "projects"]
    )
  );

// With Id

routes
  .route("/:id([0-9]+)")
  .get(
    (req, res, next) => jwtMiddleware(req, res, next, 1),
    (req, res) =>
      userController.getOneById(
        req,
        res,
        User,
        [
          "id",
          "email",
          "firstName",
          "lastName",
          "tel",
          "zipcode",
          "address",
          "city",
          "avatar_img"
        ],
        ["userRole", "projects"]
      )
  )
  .put(
    (req, res, next) => jwtMiddleware(req, res, next, 1),
    (req, res) => userController.put(req, res, User)
  )
  .delete(
    (req, res, next) => jwtMiddleware(req, res, next, 1),
    (req, res) => userController.delete(req, res, User)
  );

export default routes;
