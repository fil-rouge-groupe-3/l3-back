const rootDir = process.env.NODE_ENV === "production" ? "build/" : "src/";

module.exports = {
  type: "mariadb",
  host: process.env.DB_URL,
  port: 3306,
  username:
    process.env.NODE_ENV === "production" ? process.env.DB_USERNAME : "root",
  password:
    process.env.NODE_ENV === "production" ? process.env.DB_PASSWORD : "root",
  database:
    process.env.NODE_ENV === "production" ? process.env.DB_DATABASE : "madera",
  synchronize: true,
  logging: true,
  entities: [rootDir + "api/database/entities/**/*.{js,ts}"],
  migrations: [rootDir + "api/database/migration/*.{js,ts}"],
  subscribers: [rootDir + "api/database/subscribers/**/*.{js,ts}"],
  cli: {
    entitiesDir: "src/api/database/entities",
    migrationsDir: "src/api/database/migration",
    subscribersDir: "src/api/database/subscriber"
  }
};
