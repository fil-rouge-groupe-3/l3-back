module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: [
    "plugin:@typescript-eslint/recommended", // Uses the recommended rules from the @typescript-eslint/eslint-plugin
    "plugin:prettier/recommended",
    "prettier/@typescript-eslint",
    "prettier",
    "eslint:recommended"
  ],
  plugins: ["@typescript-eslint"],
  parserOptions: {
    parser: "@typescript-eslint/parser" // Specifies the ESLint parser
  },
  rules: {
    "prefer-const": "off",
    "no-unused-vars": "off",
    "prettier/prettier": "warn",
    "@typescript-eslint/no-unused-vars": "warn",
    "@typescript-eslint/camelcase": "off",
    "@typescript-eslint/no-empty-function": "warn",
    "no-console": process.env.NODE_ENV === "production" ? "error" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",
    "@typescript-eslint/explicit-function-return-type": "off",
    "@typescript-eslint/interface-name-prefix": ["warn", {"prefixWithI": "always"}],
    "no-tabs": [
      "error",
      {
        allowIndentationTabs: true
      }
    ]
  }
};
