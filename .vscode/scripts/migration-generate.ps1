param([string]$inputMigrationName)
Remove-Item -Path "./build" -Recurse
& "npm" "run" "build"
# Add MySQL Data Connector
[void][system.reflection.Assembly]::LoadFrom("./ext/MysqlConnector/Mysql.Data.dll")
 
# Open Connection to SQL Server
$connStr = "server=127.0.0.1;port=3306;uid=root;pwd=root"
$conn = New-Object MySql.Data.MySqlClient.MySqlConnection($connStr)
$conn.Open()

# Drop MySQL Database
Try {
    $dropmysqldatabase = 'DROP DATABASE `madera`; CREATE DATABASE IF NOT EXISTS `madera`'
    $cmd = New-Object MySql.Data.MySqlClient.MySqlCommand($dropmysqldatabase, $conn)
    $da = New-Object MySql.Data.MySqlClient.MySqlDataAdapter($cmd)
    $ds = New-Object System.Data.DataSet
    $da.Fill($ds)
}
Catch {
    $dropmysqldatabase = 'CREATE DATABASE IF NOT EXISTS `madera`'
    $cmd = New-Object MySql.Data.MySqlClient.MySqlCommand($dropmysqldatabase, $conn)
    $da = New-Object MySql.Data.MySqlClient.MySqlDataAdapter($cmd)
    $ds = New-Object System.Data.DataSet
    $da.Fill($ds)
}

& "ts-node" "./node_modules/typeorm/cli.js" "migration:run"
& "ts-node" "./node_modules/typeorm/cli.js" "migration:generate" "-n" $inputMigrationName