<h1 align="center">Bienvenue sur Madera BACK 👋</h1>

## Install Package

```sh
npm install
```

and renove the `.example` on these files `ormconfig.json.example` & `config.ts.example`

## Usage

```sh
npm run start
```